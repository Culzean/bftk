#include <iostream>
using namespace std;

//Defining the window size
#define WINDOW_WIDTH  800
#define WINDOW_HEIGHT 600

//Defining the coordinate of the openGL renderer
#define INTERNAL_WIDTH 800
#define INTERNAL_HEIGHT 600

//Maximum tile size
#define MAX_TILE_SIZE 64

//Gravity settings
#define GRAVITY 3