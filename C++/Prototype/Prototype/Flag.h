#ifndef FLAG_H
	#define FLAG_H

#include "Entity.h"
#include "Unit.h"
#include "CollisionControl.h"

class Flag: public Entity
{
public:
	Flag(int posX, int posY, int width, int height, const char entityName[] , int life, const char spriteName[], bool transparency, int ispeed, int flagPole);

	void update(Unit* currentPlayer);
	void draw();

	void setFlagSpeed(float newVal) { flagSpeed = newVal; };
	float getFlagSpeed() { return flagSpeed; };
	bool getCollected() { return collected; };
	void setCollected(bool change) { collected = change; };
	bool getTriggered() { return triggered; };
	void setTriggered(bool change) { triggered = change; };
	void setPoleHeight(int newVal) { poleHeight = newVal; };
	int getPoleHeight() { return poleHeight; };
	bool getOnTop() { return onTop; };
	void setOnTop(bool val) { onTop = val; };
	int getBaseX() { return baseX; };
	void setBaseX(int val) { baseX = val; };
	int getBaseY() { return baseY; };
	void setBaseY(int val) { baseY = val; };

private:
	int poleHeight;
	int baseX,baseY;
	float flagSpeed;
	bool collected, triggered;
	bool onTop;


};

#endif