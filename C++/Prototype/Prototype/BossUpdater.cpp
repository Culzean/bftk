#include "BossUpdater.h"

BossUpdater::BossUpdater(Level* currentLevel, Unit* player)
{
	cout << "boss constructor started " << endl; 
	bossSpawn.x_ = 640;
	bossSpawn.y_ = 70;
	toughGuy = new Boss(bossSpawn.x_, bossSpawn.y_, player);
	toughGuy->setMap(currentLevel);
	toughGuy->setFoe(true);
	bossMap = new CollisionMap(player,currentLevel);
	bossMap->addToMap(toughGuy);
	toughGuy->setTarget(player);
	bossSlow = 0;
	bossStop = 170;
	cout << "boss constructor completed " << endl; 
}

BossUpdater::~BossUpdater()
{
	delete toughGuy;
	toughGuy = NULL;
	delete bossMap;
	bossMap = NULL;
}

void BossUpdater::update()
{
	cout << "boss updater started " << endl; 
	if(toughGuy != NULL)
		{//force his friction to be high!
			toughGuy->setFriction(0.7);
			toughGuy->update();
		}

	if(bossSlow > bossStop)
	{
		bossSlow = 0;
		toughGuy->setVelocityX(0.1);
	}

	if(bossMap != NULL)
	{
		bossMap->updateMap(0);
		bossMap->playerCheck();
	}
	bossSlow += 2;
	cout << "boss updater completed " << endl; 
}

void BossUpdater::draw()
{
	if(toughGuy != NULL)
		toughGuy->draw();
}