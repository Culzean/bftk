#ifndef STATEMANAGER_H
	#define STATEMANAGER_H

	#include <vector>
	using namespace std;

	class GameState;

	class StateManager
	{
		public:
			StateManager();

			void changeState(GameState* state);
			GameState* getCurrentState() { return states.back(); }
		private:
			vector<GameState*> states;
	};
#endif