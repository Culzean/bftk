#ifndef BOSSUPDATER_H
	#define BOSSUPDATER_H

	#include "CollisionMap.h"
	#include "Boss.h"

	class BossUpdater
	{
		public:
			BossUpdater(Level* currentLevel, Unit* player);
			~BossUpdater();
			void update();
			void draw();

			Boss* getBoss() { return toughGuy; }

		private:
			SpawnPoint bossSpawn;
			SpawnPoint playerSpawn;
			SpawnPoint hoardSpawn;
			Boss* toughGuy;
			int bossStop, bossSlow;
			
			CollisionMap* bossMap;
	};

#endif