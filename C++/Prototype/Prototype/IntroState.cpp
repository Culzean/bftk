#include <stdio.h>

#include "IntroState.h"
#include "MapState.h"
#include "Game.h"

IntroState::IntroState()
{
}

void IntroState::Init()
{
	glutSetCursor(GLUT_CURSOR_INHERIT);
	//Initialising the background
	introBackground = new Bitmap("Data/Menu/Good_background.bmp", 1, false);
	playButton = new Image_Button("Data/Menu/Play_button.bmp", 335, 405);
	playButton->setPosition(335, 405);
	settingsButton = new Image_Button("Data/Menu/Settings_button.bmp", 335,340);
	settingsButton->setPosition(335, 340);
	exitButton = new Image_Button("Data/Menu/Exit_button.bmp", 335,275);
	exitButton->setPosition(335, 275);

	pWarrior = new BitmapAnimated(1, "Data/Sprites/Characters/Warrior.bmp", true);

	BitmapAnimation walk(4, 0, 64, 70, 63, 0.05, true);

	pWarrior->setAnimation(walk);
	warriorPosition = 0;
}

void IntroState::Cleanup()
{
	delete introBackground;
	delete playButton;
	delete settingsButton;
	delete exitButton;
	delete pWarrior;
}

void IntroState::Pause()
{
	printf("IntroState Pause\n");
}

void IntroState::Resume()
{
	printf("IntroState Resume\n");
}

//Events handling functions
void IntroState::HandleKeyboardEvents(unsigned char value, int x, int y, StateManager* stateManager) {}
void IntroState::HandleSpecialKeyboardEvents(int value, int x, int y, StateManager* stateManager) {}
void IntroState::HandleSpecialKeyboardUpEvents(int value, int x, int y, StateManager* stateManager) {}
void IntroState::HandleMouseEvent(int button, int state, int x, int y, StateManager* stateManager) 
{
	if(playButton->clicked(x, y))
		stateManager->changeState(new MapState());
	if(exitButton->clicked(x, y))
		exit(0);
}

void IntroState::Update(StateManager* stateManager) 
{
	warriorPosition = warriorPosition + 3;

	if(warriorPosition > INTERNAL_WIDTH)
		warriorPosition = -800;
}

void IntroState::Draw() 
{
	if(introBackground != NULL)
		introBackground->drawAt(0,0);
	if(playButton != NULL)
		playButton->draw();
	if(settingsButton != NULL)
		settingsButton->draw();
	if(exitButton != NULL)
		exitButton->draw();

	pWarrior->PlayAnimation(warriorPosition, 20);
}
