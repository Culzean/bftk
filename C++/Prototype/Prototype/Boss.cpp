#include "Boss.h"
#include "Game.h"
#include <time.h>

Boss::Boss(int posX, int posY, Unit* player):Unit(posX, posY, 200, 200, "Boss", 1000, 5, 0.1, 1, "Data/Sprites/Characters/boss.bmp", true)
{
	//Just defining the animations here (really important)
	setAnimation(0, BitmapAnimation(3, 0, 200, 200, 199, 0.05, true));
	setAnimation(1, BitmapAnimation(3, 0, 400, 200, 199, 0.05, true));
	setAnimation(2, BitmapAnimation(3, 0, 0, 200, 199, 0.05, false));
	setAttackRange(100);
	target = player;
	goIdle();
	setFriction(0.4);
}

//Called everyframe to change the animation of the warrior according to its current activity
void Boss::update()
{
	Unit::update();

	if(getTarget() != NULL && getActivity() != DIE)
		{
			int dist = getPositionX() - getTarget()->getPositionX();
			if(abs(dist) < getAttackRange())
				setMarch(ENGAGE);
			else
				{
					setMarch(CollisionControl::goRight(getPositionX(),getTarget()->getPositionX()));
					setActivity(WALK);
				}
		}

	switch (getMarch())
	{		
	case LEFT:
				//do we move left or right?

					if(canMoveLeft == true)
					{
						move('l');
						break;
					}

	case RIGHT:


					if(canMoveRight == true)
					{
						move('r');
						break;
					}

	case ENGAGE:
			
			setActivity(ATTACK);
			attack(getTarget());
				//
				break;
	}

	switch(activity)
	{
		case(IDLE): getSprite()->setAnimation(getAnimation(0));
			break;
		case(ATTACK): getSprite()->setAnimation(getAnimation(1));
			break;
		case(DIE): getSprite()->setAnimation(getAnimation(2));
		break;
	}
}