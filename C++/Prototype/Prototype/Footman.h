#ifndef FOOTMAN_H
	#define FOOTMAN_H

	#include "Unit.h"

	class Footman: public Unit
	{
		public:
			//Constructor
			Footman(int posX, int posY, bool isFoe);

			//Custom update for the mushroom
			void update();
	};
#endif