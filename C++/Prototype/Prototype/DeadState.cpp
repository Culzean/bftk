#include <stdio.h>

#include "DeadState.h"
#include "IntroState.h"


DeadState::DeadState()
{
}

void DeadState::Init()
{
	//Initialising the background
	deadBackground = new Bitmap("Data/Menu/Defeat_background.bmp", 1, false);
}

void DeadState::Cleanup()
{
	delete deadBackground;
}

void DeadState::Pause()
{
	printf("MapState Pause\n");
}

void DeadState::Resume()
{
	printf("MapState Resume\n");
}

//Events handling functions
void DeadState::HandleKeyboardEvents(unsigned char value, int x, int y, StateManager* stateManager)
{
	switch(value)
	{
		case ' ' : stateManager->changeState(new IntroState());
			break;
	}
}
void DeadState::HandleSpecialKeyboardEvents(int value, int x, int y, StateManager* stateManager) {}
void DeadState::HandleSpecialKeyboardUpEvents(int value, int x, int y, StateManager* stateManager) {}
void DeadState::HandleMouseEvent(int button, int state, int x, int y, StateManager* stateManager) {}

void DeadState::Update(StateManager* stateManager) 
{
	
}

void DeadState::Draw() 
{
	if(deadBackground != NULL)
	{
		deadBackground->drawAt(0,0);
		TextDisplayer::displayText(GLUT_BITMAP_TIMES_ROMAN_24, 285, 20, "Press Space to continue", 3);
	}
}
