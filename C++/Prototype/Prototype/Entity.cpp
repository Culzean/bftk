#include "Entity.h"

//Defining constructors
Entity::Entity(int posX, int posY, int width, int height, const char entityName[], int life)
{
	setPosition(posX, posY);
	setLife(life);
	setName(entityName);
	setSize(width, height);
	//Initialising the sprite to null (this is an invisible entity)
	sprite = NULL;
	FRICTION = 0.97;
	dx = 0; dy = 0;
	//halfWidth = 0.5 * width;
	//halfHeight = 0.5 * height;
	map = NULL;
}
Entity::Entity(int posX, int posY, int width, int height, const char entityName[], int life,const char spriteName[], bool transparency)
{
	setPosition(posX, posY);
	setLife(life);
	setName(entityName);
	setSize(width, height);
	//Initialising the bitmap animated and its shadow if needed
	sprite = new BitmapAnimated(posX, posY, 1, spriteName, transparency);
	//Setting up a basic animation using the data from the entity (might looks like shit)
	animations[0] = BitmapAnimation(0, 0, 0, width, height, 0, false);
	sprite->setAnimation(animations[0]);
	FRICTION = 0.97;
	setVelocity(0,0);
	//halfWidth = 0.5 * width;
	//halfHeight = 0.5 * height;
	map = NULL;
}

Entity::~Entity()
{
	if(getSprite() != NULL)
		{
			delete sprite;
			sprite = NULL;
		}
}

//The function called to update the visual part of the entity (drawing them only if they're != NULL)
void Entity::draw()
{
	//If the entity has a sprite then play its current animation
	if(getSprite() != NULL)
		getSprite()->PlayAnimation(getPositionX()-getWidth()/2, getPositionY());
}

//The function called to update the entity
void Entity::update() 
{
	//By default any entity play the idle animation
	getSprite()->setAnimation(getAnimation(0));

	//Euler Integration
	//update all entities' speed and position
	//make sure this does not go over the maximum velocity in their plane
	if(dx > MAX_VELOCITY)
		dx = MAX_VELOCITY;
	else if(dx < - MAX_VELOCITY)
		dx = -MAX_VELOCITY;
	if(fabs(dy) > MAX_VELOCITY)
		dy = ((dy/fabs(dy)) * MAX_VELOCITY);

	//apply friction. This can be set to 1 if not idle
	//if FRICTION is 0.9 then you take off 10% every time update is called
	dx *= FRICTION; dy *= FRICTION;

	//catch the velocity and set to zero if we have taken many 10%s away from velocity
	if(fabs(dx) < ( MAX_VELOCITY * 0.06))
		dx = 0;
	if(fabs(dy) < MAX_VELOCITY * 0.03)
		dy = 0;

	//set new position same as old position plus new dx and dy
	setPosition((getPositionX() + dx), (getPositionY() + dy));
}

//The resize functions for the sprite
void Entity::resizeSprite(double sizeSpriteX, double sizeSpriteY)
{
	if(getSprite() != NULL)
		getSprite()->resize(sizeSpriteX, sizeSpriteY);
}