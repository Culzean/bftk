#ifndef BOSS_H
	#define BOSS_H

	#include "Unit.h"
	#include "CollisionControl.h"

	class Boss: public Unit
	{
		public:
			//Constructor
			Boss(int posX, int posY, Unit* player);

			//Custom update for the mushroom
			void update();
	};
#endif