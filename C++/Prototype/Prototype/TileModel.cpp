#include "TileModel.h"
#include "Game.h"

TileModel::TileModel(int itileRow = 0, int itileColumn = 0, int imapRow = 0, int imapColumn = 0, int iwidth = MAX_TILE_SIZE, int iheight = MAX_TILE_SIZE, bool blockable = false, bool isClimbable = false, bool isFloor = false)
{
	//all with default values
	setSheetCoordinate(itileRow, itileColumn);
	setMapPosition(imapRow, imapColumn);
	//The size is used for collision detection
	setSize(iwidth, iheight);
	setPosition(imapColumn * MAX_TILE_SIZE, imapRow * MAX_TILE_SIZE);
	block = blockable;
	climbable = isClimbable;
	floor = isFloor;
}

void TileModel::display(Bitmap* rTileSheet)
{
	//yep to get the position x on the tilesheet we have to multiply the tilesheetColumn by the size of the tiles. Same for the position y but with the tilesheetRow.
	rTileSheet->drawAt(posX, posY, tileSheetColumn * MAX_TILE_SIZE, tileSheetRow * MAX_TILE_SIZE, MAX_TILE_SIZE, MAX_TILE_SIZE);
}