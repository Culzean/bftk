#ifndef INTROSTATE_H
	#define INTROSTATE_H

	#include "Bitmap/BitmapAnimated.h"
	#include "GameState.h"
	#include "Bitmap/Bitmap.h"
	#include "Image_Button.h"

	class IntroState: public GameState
	{
		public:
			IntroState();
			void Init();
			void Cleanup();

			void Pause();
			void Resume();

			//Events handling functions
			void HandleKeyboardEvents(unsigned char value, int x, int y, StateManager* stateManager);
			void HandleSpecialKeyboardEvents(int value, int x, int y, StateManager* stateManager);
			void HandleSpecialKeyboardUpEvents(int value, int x, int y, StateManager* stateManager);
			void HandleMouseEvent(int button, int state, int x, int y, StateManager* stateManager);

			void Update(StateManager* stateManager);
			void Draw();
		private:
			Bitmap* introBackground;
			Image_Button* playButton;
			Image_Button* settingsButton;
			Image_Button* exitButton;
			BitmapAnimated* pWarrior;
			int warriorPosition;
	};
#endif