#ifndef ENNEMYFOOTMAN_H
	#define ENNEMYFOOTMAN_H

	#include "Unit.h"

	class EnnemyFootman: public Unit
	{
		public:
			//Constructor
			EnnemyFootman(int posX, int posY);

			//Custom update for the mushroom
			void update();
	};
#endif