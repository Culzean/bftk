#ifndef COLLISIONMAP_H
	#define COLLISIONMAP_H

#include "Unit.h"
#include "Entity.h"
#include "AIFootman.h"
#include "GroupFormation.h"
#include "CollisionControl.h"
#include "Entity.h"
#include "GroupFormation.h"
#include <vector>

#define MAX_UNIT_DENSITY 8

class CollisionMap
{
public:
	CollisionMap(Unit* player, Level* lvl);
	~CollisionMap();
	void update(vector< AIFootman* > &ptrRostar, vector< GroupFormation* > &ptrFormations, int totalCount);
	void updateMap(int searchRef);
	void updateFormations(vector< GroupFormation* > &ptrFormations);
	bool removeFromMap(int mapRef);
	void addToMap(Unit* newUnit);
	bool findTarget(Unit* prowlUnit);
	bool playerCheck();

private:
	Unit* currentPlayer;
	Level* currentLvl;
	vector< vector< Unit* > > collisionMap;
	int currentSize;

	void bumpCheck(bool right, int mapRef, Unit* currentUnit);
	bool attackCheck(GroupFormation* currentFormation);
	void maintainForm(GroupFormation* currentFormation);
	bool updatePlayerFloor(bool onTopNow);

	//void feedDump(GroupFormation* currentFormation);
	//void feedDump(Unit* unit, int search, int recr);

	//bool toFight(vector< vector< Unit* > > &localRefMap,  int runningCount, int formationCount, int centreCol, int searchCol, bool isFoe);
	int getNextCol(int preCol, int centre, int recrLvl);

	void drawDebug();

};


#endif