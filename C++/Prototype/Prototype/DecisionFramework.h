// DecisionFramework.h: interface for the CDecisionFramework class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DECISIONFRAMEWORK_H__12DB0268_8C24_4FD1_AC77_3A33FE88616B__INCLUDED_)
#define AFX_DECISIONFRAMEWORK_H__12DB0268_8C24_4FD1_AC77_3A33FE88616B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector>
#include "Threshold.h"
//#include "stdafx.h"

typedef unsigned int uint;								// retyped for convenience

struct THRESHOLD_ENTRY {								// Used by the vector THRESHOLD_ENTRY
	CThreshold* pThreshold;
	uint Priority;
};							

typedef std::vector<THRESHOLD_ENTRY> THRESHOLD_LIST;	// List of thresholds in this Decision Framework


////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////

class CDecisionFramework  
{
public:
	/////////////////////////////////////////////////////////////////////
	// Construction/Destruction
	
	CDecisionFramework(	int MaxX, 
						int MaxY);	// Constructor must define axis maximums

	virtual ~CDecisionFramework();

	/////////////////////////////////////////////////////////////////////
	// Result information
	
	bool PointInResultZone(	double X, 
							double Y, 
							uint Index = 0 );	// Is point in the result zone defined by the threshold?

	bool PointInResultZone(	DATA_POINT Point, 
							uint Index = 0 );	// Is point in the result zone defined by the threshold?
	
	double GetDepth(	DATA_POINT Point, 
						THRESH_DISTANCE_TYPE DistanceType, 
						uint Index = 0 );					// How far is the point into the result zone? Value depends on DistanceType

	double GetDepth(	double X, 
						double Y, 
						THRESH_DISTANCE_TYPE DistanceType, 
						uint Index = 0 );					// How far is the point into the result zone? Value depends on DistanceType

	int GetHighestPriorityThresholdHit( DATA_POINT Point);	// Get the threshold index where the point is in the result zone
	
	int GetHighestPriorityThresholdHit(	double X, 
										double Y );			// Get the threshold index where the point is in the result zone
	
	int GetDeepestThresholdHit(	DATA_POINT Point, 
								THRESH_DISTANCE_TYPE DistanceType ); // Get the threshold index where the point 
																	// is the deepest into the zone
	

	/////////////////////////////////////////////////////////////////////
	// Framework sizing accessors
	
	void SetMaxX( int MaxX )/* { mMaxX = MaxX; }*/;
	void SetMaxY( int MaxY )/* { mMaxY = MaxY; }*/;
	void SetMaxXandY( int MaxX, int MaxY );

	int GetMaxX() {return mMaxX;}
	int GetMaxY() {return mMaxY;}


	/////////////////////////////////////////////////////////////////////
	// Threshold creation

	uint AddThreshold(	double XInt = 100,  
						double YInt = 100,  
						int Priority = 0,  
						THRESH_RESULT_DIRECTION ResultDirection = THRESH_RESULT_UNDER,  
						THRESH_GRADE_TYPE GradientType = THRESH_GRADE_BINARY );

						// Creates a new threshold object and adds it to the TRESHOLD_LIST vector

	/////////////////////////////////////////////////////////////////////
	// Threshold accessors

	void SetThreshXInt( double X, uint Index = 0 );
	void SetThreshYInt( double Y, uint Index = 0 );
	void SetThreshXandYInt( double X, double Y, uint Index = 0 );

	void SetThreshSlopeAndX( double Slope, double X, uint Index = 0 );
	void SetThreshSlopeAndY( double Slope, double Y, uint Index = 0 );
	void SetThreshTopAndXInt( double Top, double X, uint Index = 0 );
	void SetThreshTopAndYInt( double Top, double Y, uint Index = 0 );
	void SetThreshRightAndXInt( double Right, double X, uint Index = 0 );
	void SetThreshRightAndYInt( double Right, double Y, uint Index = 0 );
	void SetThreshTopAndRight( double Top, double Right, uint Index = 0 );
	void SetThreshSlopeAndTop( double Slope, double Top, uint Index = 0 );
	void SetThreshSlopeAndRight( double Slope, double Right, uint Index = 0 );

	/////////////////////////////////////////////////////////////////////
	// Set Threshold Behaviors

	void SetThreshResultDirection(	THRESH_RESULT_DIRECTION ResultDirection, 
									uint Index = 0);	// Set the result zone over or under the threshold?

	void SetThreshGradientType(	THRESH_GRADE_TYPE GradientType, 
								uint Index = 0);		// Set the gradient to binary, or the specified direction

	void SetThreshGradientSlope(double Slope, 
								uint Index = 0);		// Set the gradient slope if it is THRESH_GRADIENT_CUSTOM

	/////////////////////////////////////////////////////////////////////
	// Get Intercept Points

	double GetThreshXInt( uint Index = 0 );
	double GetThreshYInt( uint Index = 0 );
	double GetThreshSlope( uint Index = 0 );

	double GetThreshYatX( double X, uint Index = 0 );
	double GetThreshXatY( double Y, uint Index = 0 );

	void NudgeThresh(	double Distance,  
						THRESH_DISTANCE_TYPE DistanceType = THRESH_DISTANCE_LINEAR,  
						THRESH_NUDGE_DIRECTION NudgeDirection = THRESH_NUDGE_PERP,  
						uint Index = 0 );
						// Move the threshold the specified distance in the specified direction

	/////////////////////////////////////////////////////////////////////	
	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////

private:

	/////////////////////////////////////////////////////////////////////
	// Private member variables
	
	int mMaxX;													// Max X-Axis value
	int mMaxY;													// Max Y-Axis value

	THRESHOLD_LIST mlThresholdList;								// Vector of all thresholds

	/////////////////////////////////////////////////////////////////////
	// Private utility functions

	bool IndexInBounds( uint Index ) const;		// Index exists - used in Asserts

	uint ListCount() const;						// How many thresholds?

	DATA_POINT ConvertToPoint( double X, double Y );	// Takes 2 values and converts them to DATA_POINT
												// Used in functions that take X,Y

};
#endif // !defined(AFX_DECISIONFRAMEWORK_H__12DB0268_8C24_4FD1_AC77_3A33FE88616B__INCLUDED_)
