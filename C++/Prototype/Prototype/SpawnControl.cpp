#include "SpawnControl.h"
#include "DecisionFramework.h"


//GroupFormation* formations[NO_FORMATIONS];

vector< GroupFormation* > formations;

CollisionMap* lvlMap = NULL;

int spawnCoords[NO_FORMATIONS][2] = { {300,70},//player's spawn
										{100,70},
										{700,70},
										{1200,250},
										{400,250},
									};

int flagCoords[NO_FORMATIONS][2] = { {400, 64},
										{1200, 64},
										{1600, 64},
										{900, 192},
										{1700, 192},
										};

Flag* flags[NO_FORMATIONS];


TriggerPoints* triggers[LEVEL_LARGE/10];


SpawnControl::SpawnControl(Level* currentLevel, Unit* player, GameRules* setRules)
{
	resetCounters();
	totalCount = 0;

	rostar.reserve(UNIT_LIMIT);

	currentLvl = currentLevel;

	currentPlayer = player;

	rules = setRules;

	spawnCoords[2][0] = (currentLvl->getLevelSize() * MAX_TILE_SIZE) - 1000;
	spawnCoords[3][0] = (currentLvl->getLevelSize() * MAX_TILE_SIZE) - 1000;

	for(int i = 0; i < NO_FORMATIONS ; i++)
	{
		GroupFormation* newFormation = new GroupFormation(i,0,spawnCoords[i][0],1,0,guarding,0,spawnCoords[i][0],spawnCoords[i][1]);
		if(i == 0)
			newFormation->setFoe(false);
		else
			newFormation->setFoe(true);
		formations.push_back(newFormation);
	}

	//AIFootman* blank = NULL;
	//coffin.push_back(blank);

	lvlMap = new CollisionMap(player, currentLevel);

	createDecisionFramework();
	createFlags();
	addSquad(2,true);
	playerIncr = 22;
	enemyIncr = 18;
	setTriggers();
}

SpawnControl::~SpawnControl()
{
	for(int i = 0; i < NO_FORMATIONS ; i++)
	{
		delete formations[i];
	}

	for(int i = 0; i < NO_FLAGS ; i++)
	{
		delete flags[i];
		flags[i] = NULL;
	}

	delete DF_attack;
	DF_attack = NULL;
	lvlMap->~CollisionMap();
	lvlMap = NULL;
	rules = NULL;

	//cout << "spawn controller destructor " << endl; 
}

void SpawnControl::update()
{
	if(counter > 10)
	{
		//intermitent update
		//check if we need to change state on any of the AI formations
		counter = 0;
		if(whichAI == NO_FORMATIONS)
		{
			whichAI = 0;
		}
		processOrders();
	}
	deleteEntity();

	for(int i = 0; i < rostar.size(); i++)
	{
		if(rostar[i] != NULL)
		{
			if(rostar[i]->getUnit()->getActivity() == DIE)
				removeEntity(i);
			else
				rostar[i]->think();
		}
	}

	lvlMap->updateMap(0);

	lvlMap->updateFormations(formations);


	if(lvlMap->playerCheck())
	{
		resetTargets();
	}

	//decide is we shall add more units
	updateCounters();

	counter++;
}

void SpawnControl::addEntity(int formation, bool friendOrFoe)
{
	GroupFormation* currentFormation = formations[formation];

	AIFootman* footman  = new AIFootman
		(currentFormation,friendOrFoe, currentFormation->getSpawnX(), currentFormation->getSpawnY());

	footman->getUnit()->setMap(currentLvl);
	footman->getUnit()->setFormationRef(formation);

	rostar.push_back(footman);
	totalCount++;
	currentFormation->addUnit(1);
	lvlMap->addToMap(footman->getUnit());

	////cout << "  well?  " << footman->getFormation()->getSpawnX();
}

void SpawnControl::deleteEntity()
{

		//has the death animation finished?
		//if so remove compeletely from game
		for(int i = 0; i<coffin.size(); i++)
			{
				if(coffin[i] != NULL && coffin[i]->getUnit() != NULL)
				{
					////cout << "check  " << coffin.size() << endl;
					if(!(removeCount()))
						coffin[i]->getUnit()->draw();
					else
					{
						do{
						//cout<< "This has been delete>?  " << true << endl;
						AIFootman* soldierToDelete = coffin.back();
						delete soldierToDelete;
						soldierToDelete = NULL;
						coffin.pop_back();
						}while(!coffin.empty());
					}
				}
			}
	
}

void SpawnControl::removeEntity(int unitRef)
{
	AIFootman* soldierToDelete = rostar[unitRef];
	GroupFormation* formationToEdit = soldierToDelete->getFormation();
	int soldierColumn = soldierToDelete->getUnit()->getMapColumn();

	//cout << "how's fighting?" << endl;
	rules->setEnnemyBaseHp(rules->getEnnemyBaseHp() - 70);
		//reset targets
	for(int i = 0; i < rostar.size() ; i++)
	{
		if(rostar[i] != NULL)
		{
		if(soldierToDelete->getUnit()->getFoe() != rostar[i]->getUnit()->getFoe())
			rostar[i]->getUnit()->setTarget(NULL);
		}
	}

	for(int i = 0; i < NO_FORMATIONS ; i++)
	{
		if(soldierToDelete->getUnit()->getFormationRef() != formations[i]->getReg_())
		{
			if(formations[i]->getTarget() != NULL)
				formations[i]->setTarget(NULL);
		}
	}

	coffin.push_back(soldierToDelete);
	//rostar.erase(rostar.begin() + unitRef);
	rostar[unitRef] = NULL;

	formationToEdit->removeUnit(1);
	formationToEdit = NULL;
}

void SpawnControl::resetCounters()
{
	totalCount = 0;
	counter = 0;
	whichAI = 0;

	playerIncr = 0;
	enemyIncr = 0;
	playerCount = 0;
	enemyCount = 0;
	thisTurn = 0;
	deathTimer = 300;
}

void SpawnControl::createDecisionFramework()
{
	
	DF_attack = new CDecisionFramework(currentLvl->getLevelSize(), 100);

	unsigned int ThresholdIndex = DF_attack->AddThreshold( 30 , 8 , 0,
		THRESH_RESULT_UNDER, THRESH_GRADE_PERP );

}

void SpawnControl::processOrders()
{
	GroupFormation* currentFormation = formations[whichAI];

	if(currentFormation->getCount() > 0)
	{
		//decide if there is enough strength to make an attack

		switch(currentFormation->getOrders())
			{
			case(guarding):
			
				//check if we will make a feint, start chasing or attack
				//else update the location based on where the player is
				if(currentFormation->getFoe() == false)
				{
					//This is the player's formation
					currentFormation->setCentre((currentPlayer->getPositionX()));
					////cout << "change places!";

				}else
				{
					currentFormation->setCentre((currentPlayer->getPositionX() + 400));
					float dist = abs(currentFormation->getCentre() - currentPlayer->getPositionX());
					
					float xVal = ((float)MAX_TILE_SIZE * 
							((formations[0]->getStrength() * (abs(currentPlayer->getVelocityX()) + 1))
							/ dist));
						int yVal = currentFormation->getStrength();
					
						//cout << "&&&& xValue  - " << xVal << "  yValue  - " << yVal << "  ***";

						if(DF_attack->PointInResultZone(xVal,yVal,0))
						{
							currentFormation->setOrders(attacking);
							//cout << "  FreeDOm " << currentFormation->getStrength();
						}else
						{
							currentFormation->setCentre((currentPlayer->getPositionX() + currentFormation->getWidth() + 100));
							//cout << " COMpuTer   " << whichAI << "  **" << currentFormation->getStrength();					
						}
					
				}


				break;

			case(attacking):

				//find another formation to go after
				//if there isn't one in range go back to guarding

				for(int i = 0; i< NO_FORMATIONS; i++)
				{
					if((formations[i]->getCount() > 0) &&
						(formations[i]->getFoe() != currentFormation->getFoe()))
					{
						//a potential target
						
								if(CollisionControl::objectOverlap
									(currentFormation->getCentre(), formations[i]->getCentre(), 150))
										
									{
										////cout << "  MISERY  - " << currentFormation->getReg_();
											//currentFormation->setGroupTarget(formations[i]);
									}else
								{
									currentFormation->setGroupTarget(NULL);
									currentFormation->setOrders(guarding);

								}


					}
				}
				break;
			}

	}
	whichAI++;
}

void SpawnControl::updateCounters()
{
	
	for(int i = 0; i<NO_FLAGS; i++)
	{
		flags[i]->update(currentPlayer);
		if(flags[i]->getCollected() == true && flags[i]->getTriggered() == false)
		{
			playerIncr = playerIncr * 1.3;
			//cout << "***REINFORCEMENTS!" << endl;
			flags[i]->setTriggered(true);
			rules->setEnnemyBaseHp(rules->getEnnemyBaseHp() - 40);
		}
	}
	for(int i = 0; i<NO_TRIGGERS ; i++)
	{
		if(triggers[i]->tripped == false)
			{
				if(triggers[i]->positions <= currentPlayer->getMapColumn())
					{
						//cout << "positions" <<  endl;
						if(i == 3)
						{
							addSquad(1,true);
							triggers[i]->tripped = true;
							//cout << "RUSH!!" << endl;
						}else
						{
							//cout << "DANGER!" << endl;
							triggers[i]->tripped = true;
							enemyIncr = enemyIncr * 1.2;
						}
					}
			}
	}
	
	playerCount += playerIncr;
	enemyCount += enemyIncr;
	if(playerCount > SPAWN_COUNT)
	{
		if(!CollisionControl::onTop(currentPlayer->getMapRow()))
			addEntity(0,false);
		else
			//add to top fortmation
			;
		playerCount = 0;
	}
	if(enemyCount > SPAWN_COUNT)
	{
		//decide where to spawn him
		if(CollisionControl::onTop(currentPlayer->getMapRow()))
		{
			if(currentPlayer->getMapColumn() > currentLvl->getLevelSize() * 0.6)
				addEntity(4,true);
			else
				addEntity(3,true);
		}
		else
			addEntity(2,true);
		enemyCount = 0;
	}
}

void SpawnControl::draw()
{
	for(int i = 0; i<rostar.size(); i++)
	{
		if(rostar[i] != NULL)
		rostar[i]->show();
	}

	for(int j = 0; j < NO_FLAGS; j++)
	{
		flags[j]->draw();
	}
}

void SpawnControl::addSquad(int formation,bool enemy)
{
	for(int i = 0; i< SQUAD_COUNT ; i++)
	{
		addEntity(formation,enemy);
	}
}

void SpawnControl::createFlags()
{
	NO_FLAGS = 5;
	int startTile;
	for(int i = 0; i < NO_FLAGS; i++)
	{
		//startTile = (i%4 * MAX_TILE_SIZE);
		flags[i] = new Flag
			(flagCoords[i][0],flagCoords[i][1],64,64,"Flag" + 1, 1 ,"Data/Sprites/Characters/Flag.bmp",true, MAX_VELOCITY * 0.05, MAX_TILE_SIZE * 2);
		flags[i]->setTriggered(false);
		flags[i]->setCollected(false);
	}
}

void SpawnControl::setTriggers()
{
	NO_TRIGGERS = currentLvl->getLevelSize() / 10;

	for(int i = 0; i < NO_TRIGGERS ; i++)
	{//set default for full map size first
		triggers[i] = new TriggerPoints;
		if(i == 0)
		triggers[i]->positions = ((currentLvl->getLevelSize() * 2) / 7) ;
		else
		{
			int remains = currentLvl->getLevelSize() - triggers[i-1]->positions;
			triggers[i]->positions =  triggers[i-1]->positions + ((remains * 2) / 6);
		}
		triggers[i]->tripped = false;
		//cout << "triggers  - " << triggers[i]->positions << endl;
	}
}

bool SpawnControl::removeCount()
{
	bool remove = false;
	if(deathTimer < 0)
		{
			remove = true;
			deathTimer = 300;
		}
	else
		deathTimer -= 10;
	////cout << "test  " << remove << "  ";
	return remove;
}

void SpawnControl::resetTargets()
{
for(int i = 0; i < rostar.size() ; i++)
	{
		if(rostar[i] != NULL)
		{
			//if((currentPlayer->getFoe() != rostar[i]->getUnit()->getFoe())
			//	&& (rostar[i]->getUnit()->getOnTop() != currentPlayer->getOnTop()))
			//rostar[i]->getUnit()->setTarget(NULL);
		}
	}

for(int i = 0; i < NO_FORMATIONS ; i++)
	{

			if(formations[i]->getTarget() != NULL)
				{
					formations[i]->setTarget(NULL);
					formations[i]->setOrders(guarding);
				}
	}
}