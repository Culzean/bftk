#include <gl/freeglut.h>

#include "Game.h"
#include "StateManager.h"
#include "IntroState.h"

StateManager* stateManager = NULL;

void init()
{
	stateManager = new StateManager();
	stateManager->changeState(new IntroState());
}

//Updating the entities (position and state)
void update()
{
	stateManager->getCurrentState()->Update(stateManager);
}

//Updating the visual part of the window
void display()
{
	//Clearing the screen
    glClear(GL_COLOR_BUFFER_BIT);
	//Drawing the updated game state
	stateManager->getCurrentState()->Draw();
	//Add the updated objects to the screen
	glutSwapBuffers();
}

//Function to refresh the screen for the animation
void animateWorld(int animating)
{
	//Updating the game
	update();
	//Then draw it
	display();
	//Calling this function each 16.5 ms which is about 60 call/second
	glutTimerFunc(16.5, animateWorld, animating);
}

//Events functon
void userKeyboardPressed(unsigned char value, int x, int y)
{
	stateManager->getCurrentState()->HandleKeyboardEvents(value, x, y, stateManager);
}

void userSpecialKeyPressed(int value, int x, int y)
{
	stateManager->getCurrentState()->HandleSpecialKeyboardEvents(value, x, y, stateManager);
}

void userSpecialKeyUp(int value, int x, int y)
{
	stateManager->getCurrentState()->HandleSpecialKeyboardUpEvents(value, x, y, stateManager);
}

void userMouseKeyPressed(int button, int state, int x, int y)
{ 
   if(!state)
   { 
		stateManager->getCurrentState()->HandleMouseEvent(button, state, x, y, stateManager);
   }
}

//Main function defining the window and the application
int main(int argc, char** argv)
{
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	glutCreateWindow("Battle for the Kingdom");
	init();
	glutDisplayFunc(display);
	glutKeyboardFunc(userKeyboardPressed);
	glutSpecialFunc(userSpecialKeyPressed);
	glutSpecialUpFunc(userSpecialKeyUp);
	glutMouseFunc(userMouseKeyPressed);
	glutTimerFunc(0, animateWorld, 0);
	glutMainLoop();
	return 0;
}