#ifndef IMAGE_BUTTON_H
	#define IMAGE_BUTTON_H

	#include "Bitmap\Bitmap.h"

	class Image_Button
	{
		public:
			//Constructor
			Image_Button(const char fileName[], int button_posX, int button_posY);

			//clicked function
			bool clicked(int posX, int posY);
			void draw();
			void setPosition(int pos_X, int pos_Y);

		private:
			Bitmap* bitmap;
			int posX;
			int posY;
	};
#endif