#include <stdio.h>

#include "MapState.h"
#include "TutorialState.h"


MapState::MapState()
{
}

void MapState::Init()
{
	//Initialising the background
	mapBackground = new Bitmap("Data/Menu/MapSelection.bmp", 1, false);
	startMission = new Image_Button("Data/Menu/Marker.bmp", 580, 115);
	startMission->setPosition(580, 115);
}

void MapState::Cleanup()
{
	delete mapBackground;
}

void MapState::Pause()
{
	printf("MapState Pause\n");
}

void MapState::Resume()
{
	printf("MapState Resume\n");
}

//Events handling functions
void MapState::HandleKeyboardEvents(unsigned char value, int x, int y, StateManager* stateManager) {}
void MapState::HandleSpecialKeyboardEvents(int value, int x, int y, StateManager* stateManager) {}
void MapState::HandleSpecialKeyboardUpEvents(int value, int x, int y, StateManager* stateManager) {}
void MapState::HandleMouseEvent(int button, int state, int x, int y, StateManager* stateManager) 
{
	if(startMission->clicked(x, y))
		stateManager->changeState(new TutorialState());
}

void MapState::Update(StateManager* stateManager) 
{
	
}

void MapState::Draw() 
{
	if(mapBackground != NULL)
		mapBackground->drawAt(0,0);
	if(startMission != NULL)
	{
		TextDisplayer::displayText(GLUT_BITMAP_HELVETICA_18, 560, 102, "STAGE 1", 3);
		TextDisplayer::displayText(GLUT_BITMAP_8_BY_13, 100, 180, "Here is the map of the Kingdom of Skia.", 1);
		TextDisplayer::displayText(GLUT_BITMAP_8_BY_13, 100, 160, "It shows the territories conquered by Morghul.", 1);
		TextDisplayer::displayText(GLUT_BITMAP_8_BY_13, 100, 140, "As a Warlord, you must take back the lands", 1);
		TextDisplayer::displayText(GLUT_BITMAP_8_BY_13, 100, 125, "by the strength of steel.", 1);
		TextDisplayer::displayText(GLUT_BITMAP_8_BY_13, 100, 105, "Click the tower to begin a battle.", 1);
		TextDisplayer::displayText(GLUT_BITMAP_8_BY_13, 100, 85, "New missions will unlock a measure of ", 1);
		TextDisplayer::displayText(GLUT_BITMAP_8_BY_13, 100, 70, "your success on the battlefield.", 1);
		TextDisplayer::displayText(GLUT_BITMAP_8_BY_13, 100, 50, "Good luck for the Battle for the Kingdom !", 1);
		startMission->draw();
	}
}
