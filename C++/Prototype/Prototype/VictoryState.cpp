#include <stdio.h>

#include "VictoryState.h"
#include "IntroState.h"


VictoryState::VictoryState()
{
}

void VictoryState::Init()
{
	//Initialising the background
	victoryBackground = new Bitmap("Data/Menu/backgroundWinLvl1.bmp", 1, false);
}

void VictoryState::Cleanup()
{
	delete victoryBackground;
}

void VictoryState::Pause()
{
	printf("MapState Pause\n");
}

void VictoryState::Resume()
{
	printf("MapState Resume\n");
}

//Events handling functions
void VictoryState::HandleKeyboardEvents(unsigned char value, int x, int y, StateManager* stateManager)
{
	switch(value)
	{
		case ' ' : stateManager->changeState(new IntroState());
			break;
	}
}
void VictoryState::HandleSpecialKeyboardEvents(int value, int x, int y, StateManager* stateManager) {}
void VictoryState::HandleSpecialKeyboardUpEvents(int value, int x, int y, StateManager* stateManager) {}
void VictoryState::HandleMouseEvent(int button, int state, int x, int y, StateManager* stateManager) {}

void VictoryState::Update(StateManager* stateManager) 
{
	
}

void VictoryState::Draw() 
{
	if(victoryBackground != NULL)
	{
		victoryBackground->drawAt(0,0);
		TextDisplayer::displayText(GLUT_BITMAP_TIMES_ROMAN_24, 285, 20, "Press Space to continue", 3);
	}
}
