#ifndef AIFOOTMAN_H
	#define AIFOOTMAN_H


	#include "EnnemyFootman.h"
	#include "Footman.h"
	#include "Entity.h"
	#include "Unit.h"
	#include <string>

class GroupFormation;

	class AIFootman
	{
		public:
			AIFootman() {};
				AIFootman(GroupFormation* thisFormation, bool isFoe, int startX, int startY);
				~AIFootman();
				Footman * unit;
				void searchTarget(Unit * units);
				void attack();
				void move(char direction);
				void think();
				Unit * getUnit(){return unit;};
				void show();
				GroupFormation* getFormation() { return myGroup; };
				void setFormation(GroupFormation* assignedFormation) { myGroup = assignedFormation; };
				//void setDestination(int newVal) { dnation = newVal; };
				int getDestination() { return 0; };
				//enum ACTION {ATTACK, MOVE} action;
				//void setFormation(int newVal) { formation = newVal; };
				//int getFormation() { return formation; };

	private:
		//int dnation;
		//int formation;
		GroupFormation* myGroup;
		void setAttack();
	};

#endif

