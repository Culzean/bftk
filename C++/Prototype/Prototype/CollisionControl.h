#ifndef COLLISION_H
	#define COLLISION_H

	#include "Unit.h"


	class CollisionControl
	{
		public:
			static void unitCanMoveLeft(Unit* gameObject)
			{
				if(gameObject->leftTile().block == true && ((gameObject->getPositionX() - gameObject->getWidth()/2) < (gameObject->leftTile().getPositionX() + gameObject->leftTile().getWidth())) )
				{
					gameObject->canMoveLeft = false;
					
				}
				else
					gameObject->canMoveLeft = true;
			}

			static void unitCanMoveRight(Unit* gameObject)
			{
				if(gameObject->rightTile().block == true && ((gameObject->getPositionX() + gameObject->getWidth()/2) > gameObject->rightTile().getPositionX()) )
				{
					gameObject->canMoveRight = false;
				}
				else
					gameObject->canMoveRight = true;
			}

			static void unitOnFloor(Unit* gameObject)
			{
				if(gameObject->currentTile().floor == true || (gameObject->bottomTile().block == true && gameObject->getPositionY() < gameObject->bottomTile().getPositionY() + gameObject->bottomTile().getHeight()))
					gameObject->isOnFloor = true;
				else
					gameObject->isOnFloor = false;
			}

			static void unitCanClimbUp(Unit* gameObject)
			{
				if(gameObject->topTile().climbable == true || gameObject->currentTile().climbable)
					gameObject->canClimbUp = true;
				else
					gameObject->canClimbUp = false;
			}

			static void unitCanClimbDown(Unit* gameObject)
			{
				if(gameObject->bottomTile().climbable == true || gameObject->currentTile().climbable)
					gameObject->canClimbDown = true;
				else
					gameObject->canClimbDown = false;
			}

			static bool objectOverlap(int x1, int x2, int width1, int width2 = 0)
			{
				//This checks if two objects on the same x plane are overlapping
				//can use a width value for each or just one overlap value
				bool returnVal = false;
				if(width2 == 0)
				{
					int dist = x1 - x2;
					if(abs(dist) <= width1)
						returnVal = true;
					else
						returnVal = false;
				}else
				{
					int dist = x1 - x2;
					if(abs(dist) <= (width1 + width2))
						returnVal = true;
					else
						returnVal = false;
				}
				return returnVal;
			}

			static ACTION goRight(int unitPositionX, int goalPositionX)
			{
				ACTION dir;
				if((unitPositionX - goalPositionX) < 0)
					dir = RIGHT;
				else
					dir = LEFT;
				return dir;
			}

			static bool onTop(int objectRow, int upStairsRow = 2)
			{
				bool top = false;
				if(objectRow > upStairsRow)
					top = true;
				return top;
			}
	};
#endif