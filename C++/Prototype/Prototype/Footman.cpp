#include "Footman.h"
#include "Game.h"

Footman::Footman(int posX, int posY, bool isFoe):Unit(posX, posY, 64, 64, "Footman", 100, 5, 0.05, 5, "Data/Sprites/Characters/Ally_Unit.bmp", true)
{
	if(isFoe)
		{//not ideal solution
			setName("Ninja");
			setSprite(BitmapAnimation(3, 0, 128, 100, 63, 0.03, true),"Data/Sprites/Characters/Ennemy_Unit.bmp",true);
		}
	
	//Just defining the animations here (really important)
	setAnimation(0, BitmapAnimation(3, 0, 128, 100, 63, 0.03, true));
	setAnimation(1, BitmapAnimation(2, 0, 64, 100, 63, 0.07, true));
	setAnimation(2, BitmapAnimation(3, 0, 192, 100, 63, 0.05, false));
	setAnimation(3, BitmapAnimation(3, 0, 0, 100, 63, 0.05, false));

	goIdle();
}

//Called everyframe to change the animation of the warrior according to its current activity
void Footman::update()
{
	Unit::update();

	switch(activity)
	{
		case(IDLE): getSprite()->setAnimation(getAnimation(0));
			break;
		case(WALK): getSprite()->setAnimation(getAnimation(1));
			break;
		case(ATTACK): getSprite()->setAnimation(getAnimation(2));
			break;
		case(DIE): getSprite()->setAnimation(getAnimation(3));
		break;
	}
}