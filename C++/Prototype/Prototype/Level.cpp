#include "Level.h"
#include <string>
using namespace std;

Level::Level()
{
	setTileSheetName("NONE");
	tileSheet = NULL;
	background = NULL;
	setFileName("");
	setLevelSize(LEVEL_SMALL);
	startColumn = 0;
	endColumn = getLevelSize();
}
Level::Level(char fileName[])
{
	Level();
	setFileName(fileName);
	load(fileName);
	nextLevelFileName = "";
}

void Level::load(char fileName[])
{
	pSpawnPoint = new PlayerSpawnPoint(200, 100);//default spawnpoint

	//Opening the file
	FILE* file = fopen(fileName, "r");

	//If file found
	if(file != NULL)
	{
		//The variables where the value will be stored
		char line[STRING_SIZE];

		char backgroundFile[STRING_SIZE];
		char sheetName[STRING_SIZE];
		int size = 0;

		//The first line define the size of the level
		fgets(line, STRING_SIZE, file); //the line About map properties
		fgets(line, STRING_SIZE, file);
		sscanf(line, "%i", &size);
		setLevelSize(size);

		//The second is for the background
		fgets(line, STRING_SIZE, file);
		sscanf(line, "%s", &line);
		sprintf(backgroundFile, "Data/Backgrounds/%s", line);
		background = new Background(backgroundFile);
		
		//Then it's the tilesheet texture
		fgets(line, STRING_SIZE, file);
		sscanf(line, "%s", &line);
		sprintf(sheetName, "Data/Tilesheets/%s", line);
		setTileSheetName(sheetName);
		tileSheet = new Bitmap(sheetName, 1, true);

		fgets(line, STRING_SIZE, file); //the line About entities

		//Variable needed for the entities
		int entityPosX, entityPosY;
		char entityType[STRING_SIZE];
		string entityTypeString;

		while(fgets(line,STRING_SIZE, file) && line[0] != '[')
		{
			sscanf(line, "%s: %i,%i", &entityType, &entityPosX, &entityPosY);
			entityTypeString = entityType;
			if(entityTypeString.compare("PlayerSpawnPoint") == 0)
			{
				delete pSpawnPoint;
				pSpawnPoint = NULL;
				pSpawnPoint = new PlayerSpawnPoint(200, 100);
			}
		}

		//Initialising all the variable to get the required data for the tiles
		int tileSheetRow = 0, tileSheetColumn = 0, mapRow = 0, mapColumn = 0, tileWidth = 0, tileHeight = 0, block = 0, climb = 0, floor = 0;

		//Then its the tiles information
		while(fgets(line,STRING_SIZE, file))
		{
			//Storing the data of the tile in the variables
			sscanf(line, "%i,%i,%i,%i,%i,%i,%i,%i,%i", &tileSheetRow, &tileSheetColumn, &mapRow, &mapColumn, &tileWidth, &tileHeight, &block, &climb, &floor);

			//Creating the tile and adding it to it's position in the array
			TileModel currentTile(tileSheetRow, tileSheetColumn, mapRow, mapColumn, tileWidth, tileHeight, block, climb, floor);
			tiles[mapRow][mapColumn] = currentTile;
		}
	}
	else
		cout << "Can't open the file:" << getFileName() << endl;

	//Closing the file
	fclose(file);
}

void Level::draw()
{
	background->drawBackground();

	for(int mapRow = 0; mapRow < LEVEL_HEIGHT ; mapRow++)
	{
		for(int mapColumn = startColumn; mapColumn < endColumn ; mapColumn++)
		{
			tiles[mapRow][mapColumn].display(tileSheet);
		}
	}
}

void Level::update(int bgPosition)
{
	background->update(bgPosition);
	startColumn = bgPosition/MAX_TILE_SIZE;
	endColumn = (bgPosition+INTERNAL_WIDTH)/MAX_TILE_SIZE + 1;
}

TileModel& Level::getTile(int tileX, int tileY)
{
	return (TileModel)tiles[tileY][tileX];
}