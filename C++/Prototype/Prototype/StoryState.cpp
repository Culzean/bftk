#include <stdio.h>

#include "StoryState.h"
#include "MapState.h"

StoryState::StoryState()
{
}

void StoryState::Init()
{
	//Initialising the background
	storyBackground = new Bitmap("Data/Menu/Story_background.bmp", 1, false);
}

void StoryState::Cleanup()
{
	delete storyBackground;
}

void StoryState::Pause()
{
	printf("MapState Pause\n");
}

void StoryState::Resume()
{
	printf("MapState Resume\n");
}

//Events handling functions
void StoryState::HandleKeyboardEvents(unsigned char value, int x, int y, StateManager* stateManager)
{
	switch(value)
	{
		case ' ' : stateManager->changeState(new MapState());
			break;
	}
}
void StoryState::HandleSpecialKeyboardEvents(int value, int x, int y, StateManager* stateManager) {}
void StoryState::HandleSpecialKeyboardUpEvents(int value, int x, int y, StateManager* stateManager) {}
void StoryState::HandleMouseEvent(int button, int state, int x, int y, StateManager* stateManager) {}

void StoryState::Update(StateManager* stateManager) 
{
	
}

void StoryState::Draw() 
{
	if(storyBackground != NULL)
	{
		storyBackground->drawAt(0,0);
		TextDisplayer::displayText(GLUT_BITMAP_TIMES_ROMAN_24, 285, 20, "Press Space to continue", 3);
	}
}
