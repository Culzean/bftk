#include "GameRules.h"
#include "PlayState.h"
#include "VictoryState.h"

GameRules::GameRules(int gametype, int Hp) 
{
	ennemyBaseHP = Hp;
	pToStateManager = NULL;
	gameType = gametype;
}

//Update function
void GameRules::update(int currentPlayerLife, StateManager* stateManager)
{
	if(currentPlayerLife <= 0)
	{
		loose(stateManager);
	}
	else
	{
		if(ennemyBaseHP <= 0)
		{
			win(stateManager);
		}
	}
}

void GameRules::win(StateManager* stateManager)
{
	if(gameType == 1)
		stateManager->changeState(new PlayState("Data/Levels/level1_Inside.txt"));
	else
		stateManager->changeState(new VictoryState());
}

void GameRules::loose(StateManager* stateManager)
{
	stateManager->changeState(new IntroState());
}