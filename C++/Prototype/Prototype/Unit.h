#ifndef UNIT_H
	#define UNIT_H

	#include "Entity.h"
	#include <string>

	using namespace std;

	enum ACTIVITIES { IDLE, WALK, ATTACK, HIT, DIE, CLIMB };

	enum ACTION {RIGHT, LEFT, SENTRY, ENGAGE};

	class Unit: public Entity
	{
		public:
			ACTIVITIES activity;

			//Constructors
			Unit(int posX, int posY, int width, int height, const char entityName[], int life, int mouvementSpeed, double attackSpeed, int attackDamage);
			Unit(int posX, int posY, int width, int height, const char entityName[], int life, int mouvementSpeed, double attackSpeed, int attackDamage, const char spriteName[], bool transparency = false);
			Unit::~Unit() { Entity::~Entity(); };	

			//Defining its possible activities
			void setActivity(ACTIVITIES newActivity) { activity = newActivity; };
			ACTIVITIES getActivity() { return activity; };

			//Get and set functions for the attributes
			void setMouvementSpeed(int newMouvementSpeed) { mouvementSpeed = newMouvementSpeed; };
			int getMouvementSpeed() { return mouvementSpeed; };
			void setAttackSpeed(double newAttackSpeed) { attackSpeed = newAttackSpeed; };
			double getAttackSpeed() { return attackSpeed; };
			void setAttackDamage(int newAttackDamage) { attackDamage = newAttackDamage; };
			int getAttackDamage() { return attackDamage; };
			void setTimerAttack(double newTimer) { timerAttack = newTimer; };
			double getTimerAttack() { return timerAttack; };
			void setIsAttacking(bool newBool) { isAttacking = newBool; };
			bool getIsAttacking() { return isAttacking; };
			int getAttackRange() { return attackRange; };
			void setAttackRange(int val) { attackRange = val; };

			//Go in idle activity
			void goIdle() { setActivity(IDLE); };
				
			//move and attack function
			void move(char direction);
			void attack();
			void attack(Unit* target);
			void climb(char upOrDown);
			//Update function
			void update();

			//AI marching orders
			void setMarch(ACTION newVal);
			void setMarch(int centre);
			ACTION getMarch() { return march; };
			//void setMarch(int centre);
			void setTarget(Unit* newFoe) { target = newFoe; };
			Unit* getTarget() { return target; };
			void setSeeRange(int newRange) { seeRange = newRange; }; //range for finding a target, measured in tiles
			int getSeeRange() { return seeRange; };
			bool getOnTop() { return onTop; };
			void setOnTop(bool val) { onTop = val; };

			//AI team assignment
			bool getFoe() { return foe; };
			void setFoe(bool newAlligence) { foe = newAlligence; };
			//number to check which group each unit belongs to
			int getFormationRef() { return formationRef; };
			void setFormationRef(int newRef) { formationRef = newRef; };

			//Booleans to check the activity of the unit
			bool isMoving;
			bool isClimbing;
			//Booleans for the collision
			bool isOnFloor;
			bool canClimbUp;
			bool canClimbDown;
			bool canMoveLeft;
			bool canMoveRight;
		protected:
			Unit * target;
		private:
			int mouvementSpeed;
			double attackSpeed;
			int attackDamage;
			double timerAttack;
			bool isAttacking;
			int seeRange;
			int attackRange;
			ACTION march;
			
			int formationRef;
			bool foe;
			bool onTop;
	};
#endif