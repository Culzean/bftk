#include <stdio.h>

#include "RulesState.h"
#include "PlayState.h"


RulesState::RulesState()
{
}

void RulesState::Init()
{
	//Initialising the background
	rulesBackground = new Bitmap("Data/Menu/Rules_background.bmp", 1, false);
}

void RulesState::Cleanup()
{
	delete rulesBackground;
}

void RulesState::Pause()
{
	printf("MapState Pause\n");
}

void RulesState::Resume()
{
	printf("MapState Resume\n");
}

//Events handling functions
void RulesState::HandleKeyboardEvents(unsigned char value, int x, int y, StateManager* stateManager)
{
	switch(value)
	{
		case ' ' : stateManager->changeState(new PlayState("Data/Levels/level1.txt"));
			break;
	}
}
void RulesState::HandleSpecialKeyboardEvents(int value, int x, int y, StateManager* stateManager) {}
void RulesState::HandleSpecialKeyboardUpEvents(int value, int x, int y, StateManager* stateManager) {}
void RulesState::HandleMouseEvent(int button, int state, int x, int y, StateManager* stateManager) {}

void RulesState::Update(StateManager* stateManager) 
{
	
}

void RulesState::Draw() 
{
	if(rulesBackground != NULL)
	{
		rulesBackground->drawAt(0,0);
		TextDisplayer::displayText(GLUT_BITMAP_TIMES_ROMAN_24, 285, 20, "Press Space to continue", 3);
	}
}
