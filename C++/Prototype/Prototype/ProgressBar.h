#ifndef PROGRESSBAR_H
	#define PROGRESSBAR_H

	#include <GL/freeglut.h>
	#include "Game.h"

	class ProgressBar
	{
		public:
			//Constructor
			ProgressBar(int x, int y, int bWidth, int bHeight, int bMaxValue, int bBorder) 
			{ 
				posX = x; 
				posY = y;
				width = bWidth;
				height = bHeight;
				currentValue = bMaxValue;
				maxValue = bMaxValue;
				border = bBorder;
			}

			void update(int camPosX, int curValue)
			{
				if(curValue >= 0)
					currentValue = curValue;

				drawWidth = (width*(((float)currentValue/maxValue)*100))/100;
				drawX = camPosX - INTERNAL_WIDTH/2 + posX;
			}

			void draw()
			{
				glBegin(GL_QUADS);
					//Back rectangle using the border
					glColor3f(0.2, 0.2, 0.2);
					glVertex2f(drawX-border, posY - border);
					glVertex2f(drawX-border + width + (border * 2), posY - border);
					glVertex2f(drawX-border + width + (border * 2), posY + height + border);
					glVertex2f(drawX-border, posY + height + border);
					//filling it
					glColor3f(1, 0, 0);
					glVertex2f(drawX, posY);
					glVertex2f(drawX + drawWidth, posY);
					glVertex2f(drawX + drawWidth, posY + height);
					glVertex2f(drawX, posY + height);					
				glEnd();
				//resetting color
				glColor4f(1, 1, 1, 1);
			}
			void setMaxValue(int newMax) { maxValue = newMax; }
		private:
			int posX, posY, drawX, maxValue, currentValue, width, height, border;
			float drawWidth;
	};
#endif