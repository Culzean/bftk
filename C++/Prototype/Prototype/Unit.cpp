#include "Unit.h"
#include "CollisionControl.h"

Unit::Unit(int posX, int posY, int width, int height, const char entityName[], int life, int mouvementSpeed, double attackSpeed, int attackDamage):Entity(posX, posY, width, height, entityName, life)
{
	setMouvementSpeed(mouvementSpeed);
	setAttackSpeed(attackSpeed);
	setAttackDamage(attackDamage);
	setTimerAttack(0);
	setIsAttacking(false);
	canMoveLeft = true;
	canMoveRight = true;
	isClimbing = false;
	isOnFloor = false;
	isMoving = false;
	canClimbUp = false;
	canClimbDown = false;
}
Unit::Unit(int posX, int posY, int width, int height, const char entityName[], int life, int mouvementSpeed, double attackSpeed, int attackDamage, const char spriteName[], bool transparency):Entity(posX, posY, width, height, entityName, life, spriteName, transparency)
{
	setMouvementSpeed(mouvementSpeed);
	setAttackSpeed(attackSpeed);
	setAttackDamage(attackDamage);
	setTimerAttack(0);
	setIsAttacking(false);
	canMoveLeft = true;
	canMoveRight = true;
	isClimbing = false;
	isOnFloor = false;
	isMoving = false;
	canClimbUp = false;
	canClimbDown = false;
}

//this function need to be redefined in the child class to select the correct animation for each activity
void Unit::update()
{

		Entity::update();

	CollisionControl::unitCanMoveLeft(this);
	CollisionControl::unitCanMoveRight(this);
	CollisionControl::unitCanClimbUp(this);
	CollisionControl::unitCanClimbDown(this);
	CollisionControl::unitOnFloor(this);

	if(getActivity() == IDLE)
		FRICTION = 0.73;
	else
		FRICTION = 0.97;

	//stop from climbing when on the floor
	if(isClimbing)
	{
		if(isOnFloor == true)
		{
			isClimbing = false;
			setPositionY(currentTile().getPositionY() + currentTile().getHeight());
		}
	}

	if(isOnFloor)
		setVelocityY(0);

	//To climb we need to be on a climbing tile (to fix when the player move on the left or the right)
	if(currentTile().climbable == false)
		isClimbing = false;

	if(isMoving)
	{
		if(dx < 0)
		{
			if(!canMoveLeft)
				setVelocityX(0);
		}
		else if (dx > 0)
		{
			if(!canMoveRight)
				setVelocityX(0);
		}
		else
			isMoving = false;
	}

	//Checking if the attack is done
	if(getIsAttacking())
	{
		if(getTimerAttack() + getAttackSpeed() <= 1)
			setTimerAttack(getTimerAttack() + getAttackSpeed());
		else
		{
			setIsAttacking(false);
			goIdle();
			setTimerAttack(0);
		}
	}

	if(getLife() < 0)
		{
			setActivity(DIE);
			setIsAttacking(false);
		}
	else if(isClimbing == true)
		setActivity(CLIMB);
	else if(getIsAttacking() == true)
		setActivity(ATTACK);
	else if(isMoving == true)
		setActivity(WALK);
	else
		setActivity(IDLE);

	//Don't apply the gravity if we're climbing
	if(isClimbing == false)
	{
		//Are we on the floor? if not apply fake gravity for now
		if(isOnFloor == false || getPositionY() > currentTile().getPositionY() + currentTile().getHeight())
			setVelocityY(getVelocityY() - GRAVITY);
		if(isOnFloor == true && getPositionY() < currentTile().getPositionY() + currentTile().getHeight())
			setPositionY(currentTile().getPositionY() + currentTile().getHeight());
	}
}

void Unit::move(char direction)
{
	//Can't walk if we're already attacking
	if(getIsAttacking() == false)
	{
		isMoving = true;

		if(direction == 'l')
		{
			if(canMoveLeft)
				setVelocityX(dx - MAX_VELOCITY * 0.1);

			getSprite()->setOrientation(MIRROR_X);
		}
		else if (direction == 'r')
		{
			if(canMoveRight)
				setVelocityX(dx + MAX_VELOCITY * 0.1);

			getSprite()->setOrientation(NORMAL);
		}
	}
}

void Unit::attack()
{
	//can't attack if not on floor or while climbing
	if(isOnFloor == true && isClimbing == false)
	{
		setIsAttacking(true);
		isMoving = false;
		setVelocityX(0);
	}
}

void Unit::attack(Unit* target)
{
	//cout << "urgh   " << getAttackDamage() << endl;
	if(target != NULL)
	{
		//cout << "doh   " << getAttackDamage() << endl;
		if(isOnFloor == true && isClimbing == false)
		{
			if(target->getOnTop() == getOnTop())
			{
				//setIsAttacking(true);
				isMoving = false;
				setVelocityX(0);
				if(getLife() > 0)
					target->setLife(target->getLife() - getAttackDamage());
			}
		}
	}
}

void Unit::climb(char upOrDown)
{
	
	if(upOrDown == 'u')
	{
		//Can I climb up? if yes
		if(canClimbUp == true)
		{
			//Checking if I'm already climbing, if no then just teleport to the top tile to start climbing
			if(isClimbing == false)
			{
				isClimbing = true;
				setPositionY(topTile().getPositionY());
			}
			else
				setPositionY(getPositionY() + 2);
		}
	}
	else if(upOrDown == 'd')
	{
		//Can I climb down? if yes
		if(canClimbDown == true)
		{
			//Checking if I'm already climbing, if no then just teleport to the bottom tile to start climbing
			if(isClimbing == false)
			{
				isClimbing = true;
				setPositionY(bottomTile().getPositionY() + bottomTile().getHeight() - 1); //cheating a bit but it works xD
			}
			else
				setPositionY(getPositionY() - 2);
		}
	}
}

void Unit::setMarch(ACTION newVal)
{
	
	march = newVal;
}

void Unit::setMarch(int centre)
{
	cout << "  " << march << "  ";
	cout << "What is this " << getMarch();
	if(getPositionX() > centre)
		setMarch(LEFT);
	else
		setMarch(RIGHT);
}
