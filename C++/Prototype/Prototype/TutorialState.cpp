#include <stdio.h>

#include "TutorialState.h"
#include "RulesState.h"


TutorialState::TutorialState()
{
}

void TutorialState::Init()
{
	//Initialising the background
	tutoBackground = new Bitmap("Data/Menu/Tuto_background.bmp", 1, false);
}

void TutorialState::Cleanup()
{
	delete tutoBackground;
}

void TutorialState::Pause()
{
	printf("MapState Pause\n");
}

void TutorialState::Resume()
{
	printf("MapState Resume\n");
}

//Events handling functions
void TutorialState::HandleKeyboardEvents(unsigned char value, int x, int y, StateManager* stateManager)
{
	switch(value)
	{
		case ' ' : stateManager->changeState(new RulesState());
			break;
	}
}
void TutorialState::HandleSpecialKeyboardEvents(int value, int x, int y, StateManager* stateManager) {}
void TutorialState::HandleSpecialKeyboardUpEvents(int value, int x, int y, StateManager* stateManager) {}
void TutorialState::HandleMouseEvent(int button, int state, int x, int y, StateManager* stateManager) {}

void TutorialState::Update(StateManager* stateManager) 
{
	
}

void TutorialState::Draw() 
{
	if(tutoBackground != NULL)
	{
		tutoBackground->drawAt(0,0);
		TextDisplayer::displayText(GLUT_BITMAP_TIMES_ROMAN_24, 285, 20, "Press Space to continue", 3);
	}
}
