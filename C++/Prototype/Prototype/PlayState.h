#ifndef PLAYSTATE_H
	#define PLAYSTATE_H

	#include "GameState.h"
	#include "GameRules.h"
	#include "HUD.h"
	#include "User.h"
	#include "Camera.h"
	#include "Level.h"
	#include "AIFootman.h"
	#include "SpawnControl.h"
	#include "BossUpdater.h"
	#include "Sounds.h"
	#include <ctime>

	class PlayState: public GameState
	{
		public:
			PlayState(char* lvlName);
			void Init();
			void Cleanup();

			void Pause();
			void Resume();

			//Events handling functions
			void HandleKeyboardEvents(unsigned char value, int x, int y, StateManager* stateManager);
			void HandleSpecialKeyboardEvents(int value, int x, int y, StateManager* stateManager);
			void HandleSpecialKeyboardUpEvents(int value, int x, int y, StateManager* stateManager);
			void HandleMouseEvent(int button, int state, int x, int y, StateManager* stateManager);

			//Update functions
			void Update(StateManager* stateManager);
			void Draw();
			void showPlayerCollision();
		private:
			int lvlType;
			HUD* pHud;
			GameRules* rules;
			User* pUser;
			Camera* pCamera;
			Level* level;
			char* levelName;
			SpawnControl* unitStore;
			BossUpdater* bossUpdater;
			bool playerCollision;
			Sounds* pSounds;
	};
#endif