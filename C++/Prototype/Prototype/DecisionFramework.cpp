// DecisionFramework.cpp: implementation of the CDecisionFramework class.
//
//////////////////////////////////////////////////////////////////////

//#include "stdafx.h"
#include "DecisionFramework.h"
#include "Threshold.h"

//#ifdef _DEBUG
//#undef THIS_FILE
//static char THIS_FILE[]=__FILE__;
//#define new DEBUG_NEW
//#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDecisionFramework::CDecisionFramework( int MaxX /*= 100*/, int MaxY /*= 100*/)
{
	mMaxX = MaxX;
	mMaxY = MaxY;
}

CDecisionFramework::~CDecisionFramework()
{
	// Clean up list memory
	
	for ( uint i; 0; ListCount() - 1 ) {
		delete mlThresholdList[i].pThreshold;
	}
}

/////////////////////////////////////////////////////////////////////
// Private utility functions

bool CDecisionFramework::IndexInBounds( uint Index ) const
{
	return ( Index <= ( ListCount() - 1 ) );
	// note "-1" is for 0-indexed vector
}

uint CDecisionFramework::ListCount() const
{
	return mlThresholdList.size();	
}

DATA_POINT CDecisionFramework::ConvertToPoint( double X, double Y )
{
	DATA_POINT Point;
	Point.X = X;
	Point.Y = Y;
	return Point;	
}

/////////////////////////////////////////////////////////////////////
// Threshold result generation

bool CDecisionFramework::PointInResultZone(	double X, 
											double Y, 
											uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "PointIsOverLine: Index not in bounds" );
	
	DATA_POINT Point = ConvertToPoint( X, Y );
	return PointInResultZone( Point, Index );
}

bool CDecisionFramework::PointInResultZone(	DATA_POINT Point, 
											uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "PointIsOverLine: Index not in bounds" );
	
	return mlThresholdList[Index].pThreshold->PointInResultZone(Point);
}

double CDecisionFramework::GetDepth(	DATA_POINT Point, 
										THRESH_DISTANCE_TYPE DistanceType, 
										uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "GetDepth: Index not in bounds" );
	
	return mlThresholdList[Index].pThreshold->GetDepth( Point, DistanceType );
}

double CDecisionFramework::GetDepth(	double X,  
										double Y,  
										THRESH_DISTANCE_TYPE DistanceType,  
										uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "GetDepth: Index not in bounds" );
	
	DATA_POINT Point =  ConvertToPoint( X, Y );
	return GetDepth( Point, DistanceType, Index );
}

int CDecisionFramework::GetHighestPriorityThresholdHit( DATA_POINT Point )
{
	int HighestIndex = -1;
	int HighestPriority = -1;
	
	for ( uint i; 0; ListCount() - 1 ) {
		
		// Note: Priority 0 thresholds are not considered
		if (	mlThresholdList[i].Priority != 0 &&
				PointInResultZone( Point, i ) ) 
		{
			if ( mlThresholdList[i].Priority > HighestPriority )	
			{
				HighestIndex = i;
				HighestPriority = mlThresholdList[i].Priority;
			}
		}
	}
	return HighestIndex;
}

int CDecisionFramework::GetHighestPriorityThresholdHit( double X, double Y )
{
	DATA_POINT Point = ConvertToPoint( X, Y );
	
	return GetHighestPriorityThresholdHit( Point );
}

int CDecisionFramework::GetDeepestThresholdHit( DATA_POINT Point, THRESH_DISTANCE_TYPE DistanceType )
{
	int DeepestIndex = -1;
	double GretestDepth = -1.0;
	double CurrentDepth;

	for ( uint i; 0; ListCount() - 1 ) 
	{
		CurrentDepth = GetDepth( Point, DistanceType, i );
		if ( CurrentDepth > GretestDepth ) 
		{
			DeepestIndex = i;
		}
	}
	
	return DeepestIndex;
}

/////////////////////////////////////////////////////////////////////
// Framework Information Accessors


void CDecisionFramework::SetMaxX( int MaxX )
{
	mMaxX = MaxX;	
}

void CDecisionFramework::SetMaxY( int MaxY )
{
	mMaxY = MaxY;	
}

void CDecisionFramework::SetMaxXandY( int MaxX, int MaxY )
{
	SetMaxX( MaxX );
	SetMaxY( MaxY );
}


/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
// Threshold accessors

uint CDecisionFramework::AddThreshold(	double XInt,  
										double YInt,  
										int Priority,  
										THRESH_RESULT_DIRECTION ResultDirection,  
										THRESH_GRADE_TYPE GradientType  )
{
	THRESHOLD_ENTRY NewThreshEntry;
	
	CThreshold* pNewThreshold = new CThreshold( this, XInt, YInt, ResultDirection, GradientType );

	NewThreshEntry.pThreshold = pNewThreshold;
	NewThreshEntry.Priority = Priority;

	mlThresholdList.push_back( NewThreshEntry );

	return mlThresholdList.size() - 1;
}

/////////////////////////////////////////////////////////////////////
// Threshold Sets


void CDecisionFramework::SetThreshTopAndXInt( double Top, double X, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "SetTopAndXInt: Index not in bounds" );
	
	mlThresholdList[Index].pThreshold->SetTopAndXInt( Top, X );
}

void CDecisionFramework::SetThreshTopAndYInt( double Top, double Y, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "SetTopAndYInt: Index not in bounds" );
	
	mlThresholdList[Index].pThreshold->SetTopAndYInt( Top, Y );	
}

void CDecisionFramework::SetThreshRightAndXInt( double Right, double X, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "SetRightAndXInt: Index not in bounds" );
	
	mlThresholdList[Index].pThreshold->SetRightAndXInt( Right, X );	
}

void CDecisionFramework::SetThreshRightAndYInt( double Right, double Y, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "SetRightAndYInt: Index not in bounds" );
	
	mlThresholdList[Index].pThreshold->SetRightAndYInt( Right, Y );	
}

void CDecisionFramework::SetThreshTopAndRight( double Top, double Right, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "SetTopAndRight: Index not in bounds" );
	
	mlThresholdList[Index].pThreshold->SetTopAndRight( Top, Right );	
}

void CDecisionFramework::SetThreshSlopeAndTop( double Slope, double Top, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "SetSlopeAndTop: Index not in bounds" );

	mlThresholdList[Index].pThreshold->SetSlopeAndTop( Slope, Top );		
}

void CDecisionFramework::SetThreshSlopeAndRight( double Slope, double Right, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "SetSlopeAndRight: Index not in bounds" );
	
	mlThresholdList[Index].pThreshold->SetSlopeAndRight( Slope, Right );		
}

void CDecisionFramework::SetThreshGradientSlope( double Slope, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "SetGradientSlope: Index not in bounds" );
	
	mlThresholdList[Index].pThreshold->SetGradientSlope( Slope );		
}

void CDecisionFramework::SetThreshXInt( double X, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "SetThreshXInt: Index not in bounds" );
	
	mlThresholdList[Index].pThreshold->SetXInt( X );	
}

void CDecisionFramework::SetThreshYInt( double Y, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "SetThreshYInt: Index not in bounds" );
	
	mlThresholdList[Index].pThreshold->SetYInt( Y );	
}

void CDecisionFramework::SetThreshXandYInt( double X, double Y, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "SetThreshXAndYInt: Index not in bounds" );
	
	mlThresholdList[Index].pThreshold->SetXandY( X, Y );
}

void CDecisionFramework::SetThreshSlopeAndX( double Slope, double X, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "SetThreshSlopeAndX: Index not in bounds" );
	
	mlThresholdList[Index].pThreshold->SetSlopeAndX( Slope, X );
}

void CDecisionFramework::SetThreshSlopeAndY( double Slope, double Y, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "SetThreshSlopeAndY: Index not in bounds" );
	
	mlThresholdList[Index].pThreshold->SetSlopeAndY( Slope, Y );	
}

void CDecisionFramework::SetThreshResultDirection( THRESH_RESULT_DIRECTION ResultDirection, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "SetThreshResultDirection: Index not in bounds" );
	
	mlThresholdList[Index].pThreshold->SetThreshResultDirection( ResultDirection );
}

void CDecisionFramework::SetThreshGradientType( THRESH_GRADE_TYPE GradientType, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "SetThreshGradientType: Index not in bounds" );
	
	mlThresholdList[Index].pThreshold->SetThreshGradientType( GradientType );
}

void CDecisionFramework::NudgeThresh(	double Distance,  
										THRESH_DISTANCE_TYPE DistanceType /*= THRESH_DISTANCE_LINEAR*/,  
										THRESH_NUDGE_DIRECTION NudgeDirection /*= THRESH_NUDGE_PERP*/,  
										uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "NudgeThresh: Index not in bounds" );
	mlThresholdList[Index].pThreshold->NudgeThresh( Distance, DistanceType, NudgeDirection );
}


/////////////////////////////////////////////////////////////////////
// Threshold Sets

double CDecisionFramework::GetThreshXInt( uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "GetThreshXInt: Index not in bounds" );

	return mlThresholdList[Index].pThreshold->GetX();		
}

double CDecisionFramework::GetThreshYInt( uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "GetThreshYInt: Index not in bounds" );
	
	return mlThresholdList[Index].pThreshold->GetY();	
}

double CDecisionFramework::GetThreshSlope( uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "GetThreshSlope: Index not in bounds" );
	
	return mlThresholdList[Index].pThreshold->GetSlope();
}

double CDecisionFramework::GetThreshYatX( double X, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "GetThreshYatX: Index not in bounds" );
	
	return mlThresholdList[Index].pThreshold->GetYatX( X );	
}

double CDecisionFramework::GetThreshXatY( double Y, uint Index /*= 0*/ )
{
	Assert ( IndexInBounds( Index ), "GetThreshXatY: Index not in bounds" );
	
	return mlThresholdList[Index].pThreshold->GetXatY( Y );		
}

