#ifndef TILEMODEL_H
	#define TILEMODEL_H

	#include "Bitmap\BitmapAnimated.h"

	#define MAX_TILE_SIZE 64

	class TileModel
	{
		public:
			TileModel(int itileRow, int itileColumn, int imapRow, int imapColumn, int iwidth, int iheight, bool block, bool climbable, bool floor);
			TileModel() {};

			//Get and set for the texture coordinate
			void setSheetCoordinate(int sheetRow, int sheetColumn) { setSheetRow(sheetRow); setSheetColumn(sheetColumn); };
			void setSheetColumn(int sheetColumn) { tileSheetColumn = sheetColumn; };
			void setSheetRow(int sheetRow) { tileSheetRow = sheetRow;};

			//Get and set for the position of the tile in the array
			void setMapPosition(int newMapRow, int newMapColumn) { setMapRow(newMapRow); setMapColumn(newMapColumn); };
			void setMapColumn(int newMapColumn) { mapColumn = newMapColumn; };
			int getMapColumn() { return mapColumn; };
			void setMapRow(int newMapRow) { mapRow = newMapRow; };
			int getMapRow() { return mapRow; };

			//Get and set position for the tile
			void setPosition(int newPosX, int newPosY) { setPositionX(newPosX); setPositionY(newPosY); };
			void setPositionX(int newPosX) { posX = newPosX; };
			int getPositionX() { return posX; };
			void setPositionY(int newPosY) { posY = newPosY; };
			int getPositionY() { return posY; };

			//Get and set for the width/height
			void setSize(int newWidth, int newHeight) { setWidth(newWidth); setHeight(newHeight); };
			void setWidth(int newWidth) { width = newWidth; };
			int getWidth() { return width; };
			void setHeight(int newHeight) { height = newHeight; };
			int getHeight() { return height; };

			//Display the tiles
			void display(Bitmap* rtileSheet);

			int getCentreX() { return posX + (width * 0.5); };
			int getCentreY() { return posY + (height * 0.5); };
			bool block, climbable, floor;
		private:
			//Texture coordinate on the tilesheet
			int tileSheetRow, tileSheetColumn;
			int mapRow, mapColumn;
			int width, height;
			//position of bottom left hand corner
			int posX, posY;
	};
#endif