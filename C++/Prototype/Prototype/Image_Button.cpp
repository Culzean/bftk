#include "Image_Button.h"
#include "Game.h"

Image_Button::Image_Button(const char fileName[], int button_posX, int button_posY)
{
	bitmap = new Bitmap(button_posX, button_posY, fileName, 1, true);
}

bool Image_Button::clicked(int cursorX, int cursorY)
{
	if(cursorX > posX && cursorX < posX+bitmap->getWidth())
	{
		if(INTERNAL_HEIGHT - cursorY > posY && INTERNAL_HEIGHT - cursorY < posY+bitmap->getHeight())
			return true;
	}
	return false;
}

void Image_Button::draw()
{
	bitmap->drawAt(posX, posY);
}

void Image_Button::setPosition(int pos_X, int pos_Y)
{
	posX = pos_X;
	posY = pos_Y;
}

