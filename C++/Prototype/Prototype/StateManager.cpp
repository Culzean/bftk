#include "StateManager.h"
#include <GL/freeglut.h>
#include "Game.h"
#include "GameState.h"

StateManager::StateManager()
{
}

void StateManager::changeState(GameState* state)
{
	//Resetting the opengl origin, didn't find another way to do it since glpushmatrix/glpopmatrix didn't work...
	glLoadIdentity();
	//Setting the background to black
	glClearColor(0.0, 0.0, 0.0, 0.0);
	//Setting the ortho axes
	gluOrtho2D(0, INTERNAL_WIDTH, 0, INTERNAL_HEIGHT);

	if(states.empty() == false)
	{
		states.back()->Cleanup();
		states.pop_back();
	}

	states.push_back(state);
	states.back()->Init();
}