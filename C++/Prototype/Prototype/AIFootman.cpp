#include "AIFootman.h"
#include "GroupFormation.h"

AIFootman::AIFootman(GroupFormation* thisFormation, bool isFoe, int startX, int startY)
{
	unit = new Footman(startX, startY, isFoe);
	unit->setTarget(NULL);
	//action = MOVE;
	setFormation(thisFormation);
	unit->setFoe(isFoe);
	unit->setMarch(RIGHT);
	setAttack();
	cout << "  Where are yuou going?  - " << thisFormation->getCentre();
}

AIFootman::~AIFootman()
{
	delete unit;
	unit = NULL;
}

void AIFootman::think()
{
	//cout << " what instruction? " << march << "  ";
	//cout << "  " << target << "  ";
		if(unit != NULL)
		unit->update();

	if(unit->getLife() <= 0)
		{
			unit->setTarget(NULL);
			unit->setActivity(DIE);
		}

	if(getUnit()->getTarget() != NULL && unit->getActivity() != DIE)
		{
			int dist = unit->getPositionX() - unit->getTarget()->getPositionX();
			if(abs(dist) < unit->getAttackRange())
				unit->setMarch(ENGAGE);
			else
				unit->setMarch(CollisionControl::goRight(unit->getPositionX(),unit->getTarget()->getPositionX()));
		}

	switch (unit->getMarch())
	{		
	case LEFT:
				//do we move left or right?

					if(unit->canMoveLeft == true)
					{
						unit->move('l');
						break;
					}

	case RIGHT:


					if(unit->canMoveRight == true)
					{
						unit->move('r');
						break;
					}

	case ENGAGE:
			
		if(unit->getTarget() == NULL)
		{
			//find a new target or return to marching
			unit->setMarch(CollisionControl::goRight(unit->getPositionX(),getFormation()->getCentre()));
		}
		else if(getFormation()->getOrders() == guarding)
		{
			unit->setTarget(NULL);
			unit->setActivity(WALK);
		}
		else
		{
			unit->setActivity(ATTACK);
			unit->attack(unit->getTarget());
		}
				//
				break;
	}

}
void AIFootman::attack()
{
	unit->attack();
}
				
void AIFootman::move(char direction)
{
	cout << "watching!!**!!";
	unit->move(direction);
}

void AIFootman::show()
{
	unit->draw();
}

void AIFootman::setAttack()
{
	if(unit->getFoe() == true)
	{
		//This is an enemy
		unit->setLife(30);
		unit->setAttackRange(unit->getWidth() * 0.65);
		unit->setAttackDamage(1);
		//unit->setAttackSpeed(2);
	}
	else
	{
		unit->setLife(50);
		unit->setAttackRange(unit->getWidth() * 0.65);
		unit->setAttackDamage(2);
		//unit->setAttackSpeed(3);
	}
}