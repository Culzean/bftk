#ifndef TEXTDISPLAYER_H
	#define TEXTDISPLAYER_H

	#include <gl/freeglut.h>

	#define COLORS_NUMBER 4
	#define RED_GREEN_BLUE 3

const float color[COLORS_NUMBER][RED_GREEN_BLUE] = {/*WHITE*/{1,1,1}, /*BLACK*/{0,0,0}, /*RED*/{1,0,0}, /*SURPRISE*/{0.92, 0.85, 0.62}};

	#define COLOR_WHITE 0
	#define COLOR_BLACK 1
	#define COLOR_RED 2
	#define COLOR_LOL 3

	/* Fonts
	The glut library provides a number of in-built fonts. These are:

	GLUT_BITMAP_9_BY_15
	GLUT_BITMAP_8_BY_13
	GLUT_BITMAP_TIMES_ROMAN_10
	GLUT_BITMAP_TIMES_ROMAN_24
	GLUT_BITMAP_HELVETICA_10
	GLUT_BITMAP_HELVETICA_12
	GLUT_BITMAP_HELVETICA_18

	The point-sizes indicated in the font names are approximate, 
	based on the size of a pixel in a standard display (which varies depending on resolution and screen size). */

	class TextDisplayer
	{
		public:
				//Constructor
				TextDisplayer();

				//Display text (*str), with font, at coordonnes x - y
				static void displayText(void *font, float x, float y, const char *str, int colorNumber)
				{	
					char *ch;
					glColor3f(color[colorNumber][0], color[colorNumber][1], color[colorNumber][2]); //print text in color (red, green, blue)
					glRasterPos3f(x, y, 0.0);
					for (ch=(char*)str; *ch ; ch++)
						glutBitmapCharacter(font, (int)*ch);
				};
	};
#endif
