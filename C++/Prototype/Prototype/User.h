#ifndef USER_H
	#define USER_H

	#include "Game.h"
	#include "Warrior.h"

	class User
	{
		public:
			User(const char playerClass[]);
			//Hero function
			void moveHero(char leftOrRight);
			void stopHero(char leftOrRight);
			void heroAttack();
			void heroClimb(char upOrDown);
			void InitHero(int posX, int posY);
			Warrior* getHero() { return pHero; };

			//Update functions
			void update();
			void show();
		private:
			const char* playerClass;
			Warrior* pHero;
	};
#endif