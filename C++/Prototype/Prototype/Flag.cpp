#include "Flag.h"


Flag::Flag(int posX, int posY, int width, int height, const char entityName[] , int life, const char spriteName[], bool transparency, int ispeed, int flagPole)
:Entity(posX, posY, width, height, entityName, life, spriteName, transparency)
{
	setFlagSpeed(ispeed);
	setPoleHeight(flagPole);
	setCollected(false);
	setBaseX(posX);
	setBaseY(posY);
	setOnTop(CollisionControl::onTop(getMapRow()));
	setAnimation(0, BitmapAnimation(4,0,192,MAX_TILE_SIZE,MAX_TILE_SIZE,0.07,true));

	getSprite()->setAnimation(getAnimation(0));
	setFriction(1);
	setVelocityY(MAX_VELOCITY * 0.5);
}

void Flag::update(Unit* currentPlayer)
{
	Entity::update();
	if(getPositionY() > (getBaseY() + getPoleHeight()))
		{
			setVelocityY(0);
			setPositionY(getBaseY() + getPoleHeight());
		}
	
	if(getOnTop() == currentPlayer->getOnTop())
	{
		if(getMapColumn() == currentPlayer->getMapColumn() && 
			abs(currentPlayer->getVelocityX()) < MAX_VELOCITY * 0.03)
		{//move down
			if(getPositionY() > getBaseY())
				setVelocityY(-MAX_VELOCITY * 0.03);
			else
				{
					setVelocityY(0);
					setCollected(true);
				}
		}
	}
	else
	{
		if(getVelocityY() < 0)
		setVelocityY(0);
	}
}

void Flag::draw()
{//draw flag pole?
	glLineWidth(5.0f);
	glBegin(GL_LINE_LOOP);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_BLEND);
		glColor3f(0.47, 0.2, 0.1);
		glVertex2i(getBaseX() - 28,getBaseY());
		glVertex2i(getBaseX() - 28,getBaseY() + getPoleHeight() + MAX_TILE_SIZE);
	glEnd();
	if(!getCollected())
	Entity::draw();
}