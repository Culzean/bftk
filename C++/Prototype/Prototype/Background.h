#ifndef BACKGROUND_H
	#define BACKGROUND_H

	#include "Bitmap\Bitmap.h"

	class Background: public Bitmap
	{
		public:
			//Constructor
			Background(const char fileName[]);

			//Update function
			void update(int cameraPosition);
			void drawBackground();
		private:
			int posX, textStart;
	};
#endif