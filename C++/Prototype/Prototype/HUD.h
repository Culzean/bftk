#ifndef HUD_H
	#define HUD_H

	#include "ProgressBar.h"
	#include "TextDisplayer.h"

	class HUD
	{
		public:
			//Constructor
			HUD(int playerLife, int ennemyBaseLife) 
			{
				pPlayerHealthBar = new ProgressBar(20, 560, 100, 15, playerLife, 2);
				pEnnemyBaseHealthBar = new ProgressBar(INTERNAL_WIDTH - 120, 560, 100, 15, ennemyBaseLife, 2);
			}
			~HUD()
			{
				delete pPlayerHealthBar;
				pPlayerHealthBar = NULL;
				delete pEnnemyBaseHealthBar;
				pPlayerHealthBar = NULL;
			}
			//Update function
			void update(int camPosition, int currentPlayerLife, int currentBaseLife)
			{
				cameraPosition = camPosition;
				pPlayerHealthBar->update(cameraPosition, currentPlayerLife);
				pEnnemyBaseHealthBar->update(cameraPosition, currentBaseLife);
			}
			void draw()
			{
				TextDisplayer::displayText(GLUT_BITMAP_9_BY_15, cameraPosition - INTERNAL_WIDTH/2 + 20, 580, "Hero health", 1);
				pPlayerHealthBar->draw();
				TextDisplayer::displayText(GLUT_BITMAP_9_BY_15, cameraPosition + INTERNAL_WIDTH/2 - 120, 580, "Ennemy health", 1);
				pEnnemyBaseHealthBar->draw();
			}
		private:
			ProgressBar* pPlayerHealthBar;
			ProgressBar* pEnnemyBaseHealthBar;
			int cameraPosition;
	};
#endif