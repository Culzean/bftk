// Threshold.cpp: implementation of the CThreshold class.
//
//////////////////////////////////////////////////////////////////////

//#include "stdafx.h"
#include "Threshold.h"
#include "DecisionFramework.h"

//#ifdef _DEBUG
//#undef THIS_FILE
//static char THIS_FILE[]=__FILE__;
//#define new DEBUG_NEW
//#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CThreshold::CThreshold()
{

}

CThreshold::CThreshold(	CDecisionFramework* pDecFrame, 
						double XInt, 
						double YInt, 
 						THRESH_RESULT_DIRECTION ResultDirection,
						THRESH_GRADE_TYPE GradientType,
						double CustomSlope )
{
	mpParentDecFrame = pDecFrame;
	mThreshGradientType = GradientType;	
	mThreshResultDirection = ResultDirection;
	SetXandY( XInt, YInt );
	
	if ( GradientType == THRESH_GRADE_CUSTOM ) 
	{
		mGradientSlope = CustomSlope;
	} 
	else 
	{
		SetGradientSlope();
	}
}


CThreshold::~CThreshold()
{

}

//////////////////////////////////////////////////////////////////////
// Public Accessors
//////////////////////////////////////////////////////////////////////

void CThreshold::SetXInt( double X )
{
	mThreshXInt = X;
	ProcessSlopes();
	return;
}

void CThreshold::SetYInt( double Y )
{
	mThreshYInt = Y;
	ProcessSlopes();
	return;
}

void CThreshold::SetXandY( double X, double Y ) 
{
	mThreshXInt = X;
	mThreshYInt = Y;
	ProcessSlopes();
}

void CThreshold::SetSlopeAndX( double Slope, double X )
{
	mThreshXInt = X;
	mThreshSlope = Slope;
	mThreshYInt = GetNewYIntercept();
	SetGradientSlope();
}

void CThreshold::SetSlopeAndY( double Slope, double Y )
{
	mThreshYInt = Y;
	mThreshSlope = Slope;
	mThreshXInt = GetNewXIntercept();
	SetGradientSlope();
}

void CThreshold::SetTopAndXInt( double Top, double X )
{
	double Slope;
	DATA_POINT TopPoint;

	TopPoint.X = Top;
	TopPoint.Y = GetMaxY();
	
	Slope = CalcSlope( TopPoint, X, 0.0 );
	mThreshXInt = X;
	mThreshYInt = CalcYInt( TopPoint, Slope );
	ProcessSlopes();	
}

void CThreshold::SetTopAndYInt( double Top, double Y )
{
	double Slope;
	DATA_POINT TopPoint;

	TopPoint.X = Top;
	TopPoint.Y = GetMaxY();
	
	Slope = CalcSlope( TopPoint, 0.0, Y );
	mThreshYInt = Y;
	mThreshXInt = CalcXInt( TopPoint, Slope );
	ProcessSlopes();	
}

void CThreshold::SetRightAndXInt( double Right, double X )
{
	double Slope;
	DATA_POINT RightPoint;

	RightPoint.X = Right;
	RightPoint.Y = GetMaxY();

	Slope = CalcSlope( RightPoint, X, 0 );
	mThreshXInt = X;
	mThreshYInt = CalcYInt( RightPoint, Slope );
	ProcessSlopes();
}

void CThreshold::SetRightAndYInt( double Right, double Y )
{
	double Slope;
	DATA_POINT RightPoint;

	RightPoint.X = Right;
	RightPoint.Y = GetMaxY();

	Slope = CalcSlope( RightPoint, 0.0, Y);
	mThreshYInt = Y;
	mThreshXInt = CalcXInt( RightPoint, Slope );
	ProcessSlopes();
}

void CThreshold::SetTopAndRight( double Top, double Right )
{
	double Slope; 
	DATA_POINT TopPoint, RightPoint;

	TopPoint.X = Top;
	TopPoint.Y = GetMaxY();

	RightPoint.X = GetMaxX();
	RightPoint.Y = Right;

	Slope = CalcSlope( TopPoint, RightPoint );
	CalcXAndYInt( TopPoint, Slope );
	ProcessSlopes();
}

void CThreshold::SetSlopeAndTop( double Slope, double Top )
{
	DATA_POINT TopPoint;

	TopPoint.X = Top;
	TopPoint.Y = GetMaxY();

	CalcXAndYInt( TopPoint, Slope );
	ProcessSlopes();
}

void CThreshold::SetSlopeAndRight( double Slope, double Right )
{
	DATA_POINT RightPoint;

	RightPoint.X = Right;
	RightPoint.Y = GetMaxX();

	CalcXAndYInt( RightPoint, Slope );
	ProcessSlopes();
}

void CThreshold::SetThreshResultDirection( THRESH_RESULT_DIRECTION ResultDirection )
{
	mThreshResultDirection = ResultDirection;
	SetCornerPoint();
}

void CThreshold::SetThreshGradientType( THRESH_GRADE_TYPE ThreshGradientType )
{
	mThreshGradientType = ThreshGradientType;
	SetCornerPoint();
}

void CThreshold::SetGradientSlope( double Slope )
{
	mGradientSlope = Slope;	
}


UINT CThreshold::GetMaxX() const
{
	return mpParentDecFrame->GetMaxX();
}

UINT CThreshold::GetMaxY() const
{
	return mpParentDecFrame->GetMaxY();	
}


//////////////////////////////////////////////////////////////////////
// Private Change Functions
//////////////////////////////////////////////////////////////////////

void CThreshold::ProcessSlopes()
{
	if ( mThreshXInt == 0.0 ) 
	{
		mThreshSlope = 9999;
		SetThreshOrientation( THRESH_SLOPE_VERT );
	} 
	else 
	{
		mThreshSlope = -1.0 * ( mThreshYInt / mThreshXInt );
		if		( mThreshSlope < 0.0 ) { SetThreshOrientation( THRESH_SLOPE_DOWN ); } 
		else if ( mThreshSlope > 0.0 ) { SetThreshOrientation( THRESH_SLOPE_UP ); }
		else	/*mSlope = 0*/		   { SetThreshOrientation( THRESH_SLOPE_HORIZ ); }
	}

	SetGradientSlope();

	return;
}

double CThreshold::GetNewYIntercept() const
{
	return ( -1 * ( mThreshSlope * mThreshXInt ) );
}

double CThreshold::GetNewXIntercept() const
{
	if ( mThreshSlope == 0 ) 
	{
		return ( -1 * ( mThreshYInt / 9999 ) );
	} 
	else 
	{
		return ( -1 * ( mThreshYInt / mThreshSlope ) );
	}

}

void CThreshold::SetThreshOrientation( THRESH_SLOPE_ORIENT Orientation )
{
	mThreshOrientation = Orientation;
	SetCornerPoint();
}

double CThreshold::CalcSlope( DATA_POINT PointA, DATA_POINT PointB )
{
	return ( PointA.Y - PointB.Y ) / ( PointA.X - PointB.X );
}

double CThreshold::CalcSlope( DATA_POINT PointA, double X, double Y )
{
	DATA_POINT PointB;
	PointB.X = X;
	PointB.Y = Y;
	return CalcSlope( PointA, PointB );
}
//////////////////////////////////////////////////////////////////////
// Test Functions
//////////////////////////////////////////////////////////////////////

bool CThreshold::PointInResultZone( DATA_POINT DataPoint ) const
{
	double ThreshY = GetYatX( DataPoint.X );
	
	if ( mThreshResultDirection == THRESH_RESULT_OVER ) 
	{
		return ( DataPoint.Y >= ThreshY );
	} 
	else 
	{	// THRESH_RESULT_UNDER
		return ( DataPoint.Y <= ThreshY );
	}	
}


double CThreshold::GetYatX( double X ) const
{	
	return mThreshSlope * X + mThreshYInt;
}

double CThreshold::GetXatY( double Y ) const
{
	return ( Y - mThreshYInt ) / mThreshSlope;
}

//////////////////////////////////////////////////////////////////////
// Fuzzy Generation Functions
//////////////////////////////////////////////////////////////////////


void CThreshold::ReCalcData()
{
	GetInverseSlope();	
	SetPerpPoint();
	GetMaxDepth();
}

double CThreshold::GetInverseSlope() const
{
	return ( -1 / mThreshSlope );
}

void CThreshold::SetPerpPoint()
{
	mPerpPoint = GetNearestThreshPoint( mCornerPoint, mGradientSlope );
}

double CThreshold::GetMaxDepth()
{
	double Length = CalcDistanceBetweenPoints( mCornerPoint, mPerpPoint );
	return Length;
}
double CThreshold::CalcDistanceBetweenPoints( DATA_POINT PointA, DATA_POINT PointB )
{
	double XDelta = PointA.X - PointB.X;
	double YDelta = PointA.Y - PointB.Y;
	
	double Distance = sqrt( ( XDelta * XDelta ) + ( YDelta * YDelta ) );
	return Distance;
}

void CThreshold::SetCornerPoint()
{
	switch ( mThreshOrientation ) {
	case THRESH_SLOPE_DOWN:
		if ( mThreshResultDirection == THRESH_RESULT_UNDER ) 
		{
			mCornerPoint.X = 0;
			mCornerPoint.Y = 0;
		} 
		else 
		{
			mCornerPoint.X = GetMaxX();
 			mCornerPoint.Y = GetMaxY();
		}
		break;

	case THRESH_SLOPE_UP:
		if ( mThreshResultDirection == THRESH_RESULT_UNDER ) 
		{
			mCornerPoint.X = GetMaxX();
			mCornerPoint.Y = 0;
		} 
		else 
		{
			mCornerPoint.X = 0;
			mCornerPoint.Y = GetMaxY();
		}
		break;
	
	case THRESH_SLOPE_HORIZ:
		if ( mThreshResultDirection == THRESH_RESULT_UNDER )  
		{
			mCornerPoint.X = 0;
			mCornerPoint.Y = 0;
		} 
		else 
		{
			mCornerPoint.X = GetMaxX();
 			mCornerPoint.Y = GetMaxY();
		}
		break;
	
	case THRESH_SLOPE_VERT:
		if ( mThreshResultDirection == THRESH_RESULT_UNDER ) 
		{
			mCornerPoint.X = 0;
			mCornerPoint.Y = 0;
		} 
		else 
		{
			mCornerPoint.X = GetMaxX();
 			mCornerPoint.Y = GetMaxY();
		}
	}	
}

double CThreshold::CalcDistanceFromThreshold( DATA_POINT Point, THRESH_GRADE_TYPE GradientType )
{
	DATA_POINT NearestPoint;
	double Distance;
	
	NearestPoint = GetNearestThreshPoint( Point, mGradientSlope );	
	Distance = CalcDistanceBetweenPoints( Point, NearestPoint );
	return Distance;
}

DATA_POINT CThreshold::GetNearestThreshPoint( DATA_POINT DataPoint, double GradeSlope )
{
	DATA_POINT NearestPoint;

	double GradeYInt = CalcYInt( DataPoint, GradeSlope );

	NearestPoint.X = ( GradeYInt - mThreshYInt ) / ( mThreshSlope - GradeSlope );	
	NearestPoint.Y = GradeSlope * NearestPoint.X + GradeYInt;
	return NearestPoint;
}

double CThreshold::CalcYInt( DATA_POINT DataPoint, double Slope )
{
	double YInt;
	
	YInt = DataPoint.Y - ( Slope * DataPoint.X );

	return YInt;
}

double CThreshold::CalcXInt( DATA_POINT DataPoint, double Slope )
{
	double XInt;

	XInt = ( -1.0 * mThreshYInt ) / Slope;
	
	return XInt;
}

void CThreshold::SetGradientSlope()
{
	switch( mThreshGradientType )
	{
	case THRESH_GRADE_PERP:
		mGradientSlope = GetInverseSlope();
		break;
		
	case THRESH_GRADE_CUSTOM:
		break;
		
	case THRESH_GRADE_HORIZ:
		mGradientSlope = 0.0;
		break;

	case THRESH_GRADE_VERT:
		mGradientSlope = 9999.0;
	    break;

	default:
		mGradientSlope = 0.0; //Binary solution set, so this doesn't matter
		break;
	}
}

double CThreshold::GetDepth( DATA_POINT DataPoint,  THRESH_DISTANCE_TYPE DistanceType /*= THRESH_DISTANCE_LINEAR */ ) 
{
	double Distance;

	if ( PointInResultZone( DataPoint ) ) 
	{
		switch(DistanceType) 
		{
		case THRESH_DISTANCE_LINEAR:
			Distance = CalcDistanceFromThreshold( DataPoint, mThreshGradientType );
			break;

		case THRESH_DISTANCE_PERCENT:
			if ( mMaxDepth != 0 )	
			{
				Distance = CalcDistanceFromThreshold( DataPoint, mThreshGradientType ) / mMaxDepth;
			} 
			else 
			{
				Distance = 100.0;
			}		
			break;

		default:
			break;
		}	
	} 
	else 
	{		
		Distance = 0.0;
	}
	
	return Distance;
}

void CThreshold::NudgeThresh(	double Distance,  
								THRESH_DISTANCE_TYPE DistanceType /*= THRESH_DISTANCE_LINEAR*/, 
								THRESH_NUDGE_DIRECTION NudgeDirection /*= THRESH_NUDGE_PERP*/ )
{
	double NudgeDistance;
	double NudgeSlope;
	double DeltaX;
	double DeltaY;

	DATA_POINT DataPoint;
	
	if ( DistanceType == THRESH_DISTANCE_PERCENT ) 
	{
		if ( mMaxDepth == 0 )	
		{
			// avoid divide by zero error
			NudgeDistance = 0;
		} 
		else 
		{
			// when using %, distance must be +/- 0.xxx
			NudgeDistance = Distance / mMaxDepth;
		}		
	} 
	else 
	{
		NudgeDistance = Distance;
	}
	
	switch( NudgeDirection ) 
	{
	case THRESH_NUDGE_PERP:
		NudgeSlope = mGradientSlope;
		break;

	case THRESH_NUDGE_HORIZ:
		NudgeSlope = 0.0;
		break;

	case THRESH_NUDGE_VERT:
	    NudgeSlope = 9999.0;
		break;

	default:
		Assert( 0, "NudgeThresh: switch statement fell through to default" );
	    break;
	}

	DataPoint.X = mThreshXInt;
	DataPoint.Y = 0;

	DeltaX = NudgeDistance * ( 1 / NudgeSlope ) ;
	DeltaY = NudgeDistance * NudgeSlope;
	
	DataPoint.X += DeltaX;
	DataPoint.Y += DeltaY;

	mThreshYInt = DataPoint.Y - ( mThreshSlope * DataPoint.X );
	mThreshXInt = GetNewXIntercept();	
}

void CThreshold::CalcXAndYInt( DATA_POINT DataPoint, double Slope )
{
	mThreshYInt = CalcYInt( DataPoint, Slope );
	mThreshXInt = CalcXInt( DataPoint, Slope );
}

