#ifndef ENTITY_H
	#define ENTITY_H
	
	#include "Level.h"
	#include "Bitmap/BitmapAnimated.h"

	#define MAX_ANIMATION 20

	#define MAX_VELOCITY 6

	class Entity
	{
		public:
			//Constructors
			Entity(int posX, int posY, int width, int height, const char entityName[], int life);
			Entity(int posX, int posY, int width, int height, const char entityName[], int life,const char spriteName[], bool transparency = false);
			~Entity();
			//Get and set functions for the position attributes of the entity
			void setPosition(int newPosX, int newPosY) { setPositionX(newPosX); setPositionY(newPosY); };
			void setVelocity(float new_dx, float new_dy) { dx = new_dx; dy = new_dy; };
			void setPositionX(int newPosX) { posX = newPosX; };
			void setPositionY(int newPosY) { posY = newPosY; };
			int getVelocityX() { return dx; };
			int getVelocityY() { return dy; };
			int getPositionX() { return posX; };
			int getPositionY() { return posY; };
			void setVelocityY(float new_dy) { dy = new_dy; };
			void setVelocityX(float new_dx) { dx = new_dx; };

			//Getting currentTile based on the center of the sprite
			TileModel currentTile() { return map->getTile(posX/MAX_TILE_SIZE, posY/MAX_TILE_SIZE); }

			TileModel leftTile() { return map->getTile((posX/MAX_TILE_SIZE) - 1, posY/MAX_TILE_SIZE); }
			TileModel rightTile() { return map->getTile((posX/MAX_TILE_SIZE) + 1, posY/MAX_TILE_SIZE); }

			TileModel topTile() { return  map->getTile(posX/MAX_TILE_SIZE, posY/MAX_TILE_SIZE + 1); }
			TileModel bottomTile() { return map->getTile(posX/MAX_TILE_SIZE, posY/MAX_TILE_SIZE - 1); }

			int getMapRow() { return posY / MAX_TILE_SIZE; }
			int getMapColumn() { return posX / MAX_TILE_SIZE; }

			//Get and set functions for the name attribute
			void setName(const char newName[]) { entityName = newName; }
			const char* getName() { return entityName; }
			//Get and set functions for the life attribute
			void setLife(int newLife) { life = newLife; }
			int getLife() { return life; }
			//Get and set for FRICTION
			void setFriction(float newFric) { FRICTION = newFric; }
			float getFriction() { return FRICTION; }


			//Get and set for the width and height of the entity (for the collision of the entity)
			void setSize(int width, int height) { setWidth(width); setHeight(height); };
			void setWidth(int newWidth) { width = newWidth; };
			void setHeight(int newHeight) { height = newHeight; };
			int getWidth() { return width; };
			int getHeight() { return height; };

			//Get and set functions for the sprite attribute
			void setSprite(BitmapAnimation animations, const char spriteName[], bool transparency) 
			{ 
				delete sprite;
				sprite = new BitmapAnimated(animations, 1, spriteName, transparency);
			};
			void removeSprite() { delete sprite; sprite = NULL; }
			BitmapAnimated* getSprite() { return sprite; }

			//The functions to update the entity (draw for visual and update for its activity)
			void draw();
			void update();

			//The function to resize the sprite and the shadow
			void resizeSprite(double sizeSpriteX, double sizeSpriteY);

			//Get and set for the animations
			void setAnimation(int position, BitmapAnimation animation) { animations[position] = animation; }
			BitmapAnimation getAnimation(int position) { return animations[position]; }

			//We need the map to check the collision
			Level* map;
			void setMap(Level* newMap) { map = newMap; }
		protected:
			float dx, dy;
			float FRICTION;
		private:
			float posX, posY;
			int width, height;
			//int halfWidth, halfHeight;
			const char* entityName;
			int life;
			BitmapAnimated* sprite;
			BitmapAnimation animations[MAX_ANIMATION];
	};
#endif