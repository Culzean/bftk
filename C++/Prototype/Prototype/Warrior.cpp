#include "Warrior.h"
#include "Game.h"

Warrior::Warrior(int posX, int posY):Unit(posX, posY, 64, 64, "Warrior", 200, 5, 0.05, 10, "Data/Sprites/Characters/warrior.bmp", true)
{
	//Just defining the animations here (really important)
	setAnimation(0, BitmapAnimation(2, 0, 0, 70, 63, 0.03, true));
	setAnimation(1, BitmapAnimation(4, 0, 64, 70, 63, 0.07, true));
	setAnimation(2, BitmapAnimation(3, 0, 128, 70, 63, 0.05, false));
	setAnimation(3, BitmapAnimation(1, 140, 0, 70, 63, 0, false));
	setAnimation(4, BitmapAnimation(3, 0, 192, 70, 63, 0, false));

	goIdle();
}

//Called everyframe to change the animation of the warrior according to its current activity
void Warrior::update()
{
	Unit::update();

	switch(activity)
	{
		case(IDLE): getSprite()->setAnimation(getAnimation(0));
			break;
		case(WALK): getSprite()->setAnimation(getAnimation(1));
			break;
		case(ATTACK): getSprite()->setAnimation(getAnimation(2));
			break;
		case(CLIMB): getSprite()->setAnimation(getAnimation(3));
			break;
		case(DIE): getSprite()->setAnimation(getAnimation(4));
		break;
	}
}