#ifndef PLAYERSPAWNPOINT_H
	#define PLAYERSPAWNPOINT_H

	class PlayerSpawnPoint
	{
		public:
			//Constructor
			PlayerSpawnPoint(int x, int y) { posX = x; posY = y; }

			int getPositionX() { return posX; }
			int getPositionY() { return posY; }
		private:
			int posX, posY;
	};
#endif