#include <stdio.h>

#include "PlayState.h"

PlayState::PlayState(char* lvlName)
{
	playerCollision = false;
	levelName = lvlName;
	if(strcmp(lvlName, "Data/Levels/level1.txt") == 0)
		lvlType = 1;
	else
		lvlType = 0;
}

void PlayState::Init()
{
	glutSetCursor(GLUT_CURSOR_NONE);
	//Build the level from the file
	level = new Level(levelName);

	//Initialising the player
	pUser = new User("Player");
	//Then his hero
	pUser->InitHero(level->getPlayerSpawnPoint()->getPositionX(), level->getPlayerSpawnPoint()->getPositionY());
	pUser->getHero()->setMap(level);

	//Initialising the camera
	pCamera = new Camera(0, level->getLevelSize() * MAX_TILE_SIZE);
	//And make it follow the hero of the player
	pCamera->followEntity(pUser->getHero());

	//Initialising AI class
	switch(lvlType)
	{
		case 1:
			bossUpdater = NULL;
			rules = new GameRules(lvlType, 1000);
			unitStore = new SpawnControl(level, pUser->getHero(),rules);
			pHud = new HUD(pUser->getHero()->getLife(), rules->getEnnemyBaseHp());
		break;
		case 0:
			unitStore = NULL;
			bossUpdater = new BossUpdater(level, pUser->getHero());
			rules = new GameRules(lvlType, bossUpdater->getBoss()->getLife());
			pHud = new HUD(pUser->getHero()->getLife(), bossUpdater->getBoss()->getLife());
		break;
	}

	//Initialising the sounds
	pSounds = new Sounds();

	srand((unsigned)time(0));

	//And play ambiant music and sounds
	pSounds->playSound(BACKGROUND_MUSIC);
	pSounds->playSound(BATTLE_SOUND);
}

void PlayState::Cleanup()
{
	delete pSounds;
	pSounds = NULL;
	if(bossUpdater != NULL)
	{
		delete bossUpdater;
		bossUpdater = NULL;
	}
	if(unitStore != NULL)
	{
		delete unitStore;
		unitStore = NULL;
	}
	delete pHud;
	pHud = NULL;
	delete rules;
	rules = NULL;
	delete pCamera;
	pCamera = NULL;
	delete pUser;
	pUser = NULL;
	cout << "play stoped " << endl;
}

void PlayState::Pause()
{
	printf("PlayState Pause\n");
}

void PlayState::Resume()
{
	printf("PlayState Resume\n");
}

void PlayState::HandleKeyboardEvents(unsigned char value, int x, int y, StateManager* stateManager)
{
}
void PlayState::HandleSpecialKeyboardEvents(int value, int x, int y, StateManager* stateManager)
{
	switch(value) 
	{ 
		case GLUT_KEY_LEFT: pUser->moveHero('l');
		break; 
		case GLUT_KEY_RIGHT: pUser->moveHero('r');
		break;
		case GLUT_KEY_UP: pUser->heroClimb('u');
		break;
		case GLUT_KEY_DOWN: pUser->heroClimb('d');
		break;

		//Allow to show or hide player collision with the map
		case GLUT_KEY_F1: playerCollision = !playerCollision;
		break;
		case GLUT_KEY_F2: stateManager->changeState(new PlayState("Data/Levels/level1_Inside.txt"));
		break;
		case GLUT_KEY_F3: rules->setEnnemyBaseHp(0);
		break;
	}
}
void PlayState::HandleSpecialKeyboardUpEvents(int value, int x, int y, StateManager* stateManager)
{
	switch(value) 
	{ 
		case GLUT_KEY_LEFT: pUser->stopHero('l');
		break; 
		case GLUT_KEY_RIGHT: pUser->stopHero('r');
		break;
	}
}
void PlayState::HandleMouseEvent(int button, int state, int x, int y, StateManager* stateManager)
{
	switch(button)
	{
		case GLUT_LEFT_BUTTON: pUser->heroAttack();
			int random_integer;
			int lowest=1, highest=3;
			int range=(highest-lowest)+1;
			for(int index=0; index<20; index++)
			{
				random_integer = lowest+int(range*rand()/(RAND_MAX + 1.0));
			}

			if(random_integer == 1)
				pSounds->playSound(ATTACK1_SOUND);
			else if(random_integer == 2)
				pSounds->playSound(ATTACK2_SOUND);
			else
				pSounds->playSound(ATTACK3_SOUND);
		break;
	}
}

void PlayState::Update(StateManager* stateManager) 
{
	//Updating the user
	pUser->update();

	//Updating the camera
	pCamera->update();

	//Updating the level (for the bg position)
	level->update(pCamera->getPositionX()-INTERNAL_WIDTH/2);

	//Updating the AI units
	if(lvlType==1)
		unitStore->update();
	else
		bossUpdater->update();


	//updating the user interface
	if(pHud != NULL)
	{
		switch(lvlType)
		{
			case 1:
				pHud->update(pCamera->getPositionX(), pUser->getHero()->getLife(), rules->getEnnemyBaseHp());
				//Gamerules
				rules->update(pUser->getHero()->getLife(), stateManager);
			break;
			case 0:
				pHud->update(pCamera->getPositionX(), pUser->getHero()->getLife(), bossUpdater->getBoss()->getLife());
				//Gamerules
				rules->update(pUser->getHero()->getLife(), stateManager);
				if(bossUpdater != NULL)
				{
					if(bossUpdater->getBoss()->getLife() <= 0)
						rules->setEnnemyBaseHp(bossUpdater->getBoss()->getLife());
				}
			break;
		}
	}
}

void PlayState::Draw() 
{
	//Updating the map
	level->draw();
	//Displaying the AI
	if(lvlType==1)
		unitStore->draw();
	else
		bossUpdater->draw();

	//Updating the player
	pUser->show();
	if(playerCollision)
		showPlayerCollision();
	//Draw the HUD after everything else
	pHud->draw();
}

void PlayState::showPlayerCollision()
{
	glBegin(GL_LINE_LOOP);
		glColor3f(0.0, 1.0, 0.0);
		glVertex2i(pUser->getHero()->leftTile().getPositionX(), pUser->getHero()->leftTile().getPositionY());
		glVertex2i(pUser->getHero()->leftTile().getPositionX() + pUser->getHero()->leftTile().getWidth(), pUser->getHero()->leftTile().getPositionY());
		glVertex2i(pUser->getHero()->leftTile().getPositionX() + pUser->getHero()->leftTile().getWidth(), pUser->getHero()->leftTile().getPositionY() + pUser->getHero()->leftTile().getHeight());
		glVertex2i(pUser->getHero()->leftTile().getPositionX(), pUser->getHero()->leftTile().getPositionY() + pUser->getHero()->leftTile().getHeight());
	glEnd();
	glBegin(GL_LINE_LOOP);
		glColor3f(1.0, 0.0, 0.0);
		glVertex2i(pUser->getHero()->topTile().getPositionX(), pUser->getHero()->topTile().getPositionY());
		glVertex2i(pUser->getHero()->topTile().getPositionX() + pUser->getHero()->topTile().getWidth(), pUser->getHero()->topTile().getPositionY());
		glVertex2i(pUser->getHero()->topTile().getPositionX() + pUser->getHero()->topTile().getWidth(), pUser->getHero()->topTile().getPositionY() + pUser->getHero()->topTile().getHeight());
		glVertex2i(pUser->getHero()->topTile().getPositionX(), pUser->getHero()->topTile().getPositionY() + pUser->getHero()->topTile().getHeight());
	glEnd();
	glBegin(GL_LINE_LOOP);
		glColor3f(0.0, 0.0, 1.0);
		glVertex2i(pUser->getHero()->rightTile().getPositionX(), pUser->getHero()->rightTile().getPositionY());
		glVertex2i(pUser->getHero()->rightTile().getPositionX() + pUser->getHero()->rightTile().getWidth(), pUser->getHero()->rightTile().getPositionY());
		glVertex2i(pUser->getHero()->rightTile().getPositionX() + pUser->getHero()->rightTile().getWidth(), pUser->getHero()->rightTile().getPositionY() + pUser->getHero()->rightTile().getHeight());
		glVertex2i(pUser->getHero()->rightTile().getPositionX(), pUser->getHero()->rightTile().getPositionY() + pUser->getHero()->rightTile().getHeight());
	glEnd();
	glBegin(GL_LINE_LOOP);
		glColor3f(0.5, 0.0, 0.5);
		glVertex2i(pUser->getHero()->bottomTile().getPositionX(), pUser->getHero()->bottomTile().getPositionY());
		glVertex2i(pUser->getHero()->bottomTile().getPositionX() + pUser->getHero()->bottomTile().getWidth(), pUser->getHero()->bottomTile().getPositionY());
		glVertex2i(pUser->getHero()->bottomTile().getPositionX() + pUser->getHero()->bottomTile().getWidth(), pUser->getHero()->bottomTile().getPositionY() + pUser->getHero()->bottomTile().getHeight());
		glVertex2i(pUser->getHero()->bottomTile().getPositionX(), pUser->getHero()->bottomTile().getPositionY() + pUser->getHero()->bottomTile().getHeight());
	glEnd();
}