#ifndef GROUP_FORMATION_H
	#define GROUP_FORMATION_H

#include "SpawnControl.h"

#define FORMATION_DEPTH 8

enum ORDERS {attacking = 0, guarding = 1, chasing  = 2, feint = 3 ,roiting = 4 };

struct SpawnPoint
{
	int x_, y_;

	SpawnPoint(int x = 100, int y = 70): x_(x),y_(y){}
};

class GroupFormation
	{
	public:
		ORDERS currentOrders, previousOrders;

		GroupFormation(int formationReg, int width, int centre, int strength, int unitCount,ORDERS startFormation,int formationSize, int spawnX, int spawnY);

		GroupFormation(){};

		SpawnPoint spawnPoint;

		//getters setters
		int getWidth() { return width_; };
		int getWidthTiles() { return width_ / MAX_TILE_SIZE; };
		int getCentre() { return centre_; };
		int getLeft() { return left_; };
		int getRight() { return right_; };
		int getFormationSize() { return formationSize_; };
		int getCount() { return unitCount_; };
		int getMarchDist() { return marchDist_; };
		int getStrength() { return strength_; };
		bool addSentryCount();
		int getSpawnX() { return spawnPoint.x_; };
		int getSpawnY() { return spawnPoint.y_; };

		int getLeftColumn();
		int getRightColumn();

		void setSpawnX(int valX) { spawnPoint.x_ = valX; };
		void setSpawnY(int valY) { spawnPoint.y_ = valY; };;
		void setSpawn(int valX, int valY) { spawnPoint.x_ = valX; spawnPoint.y_ = valY; };

		ORDERS getOrders () { return currentOrders; };
		ORDERS getPrvOrders () { return previousOrders; };
		void setOrders(ORDERS newOrders);
		void setTarget(Unit* newTarget) { target_ = newTarget; };
		Unit* getTarget() { return target_; };

		void setGroupTarget(GroupFormation* newTarget) { targetGroup = newTarget; };
		GroupFormation* getGroupTarget() { return targetGroup; };

		void setFoe(bool newVal) { isFoe = newVal; };
		bool getFoe() { return isFoe; };

		void addGroupVelocity(int increase) { totVel += increase; };
		int getGroupVelocity() { return totVel; };
		void resetVelocity() { totVel = 0; };

		void setWidth(int width);
		void addUnit(int count);
		void removeUnit(int count);

		void setCentre(int centre);
		int getReg_() { return reg_; };

	private:
		int reg_;
		int strength_;
		int unitCount_;
		int formationSize_;
		int formationCount_;
		int marchDist_;
		int centre_;
		int width_;
		int left_;
		int right_;
		int sentryCount, SENTRYTRIGGER;
		Unit* target_;
		GroupFormation* targetGroup;
		int totVel;
		bool isFoe;
	};

#endif