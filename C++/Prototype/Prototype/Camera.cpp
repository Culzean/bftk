/*--------------------------------------------------------------------------------------------------------------
	Copyright (C) 2011  Raphael Chappuis

						This class is used to manage the camera translation

	It'll translate according to the position given in the args.
	Map boundaries and window width need to be defined in order to know if the camera 
	can move or not.
	You can also teleport the camera to a certain point using the function teleportTo.
	It can be useful if you want to disable user control and teleport the camera to an
	entity or point on the map (i.e for cutscene).

	This camera was developped only to be used in 2d game that need an horizontal scroll.

--------------------------------------------------------------------------------------------------------------*/

#include "Camera.h"

Camera::Camera(int mapLeftBoundary, int mapRightBoundary)
{
	//Setting max and min position of the camera according to the mapboundaries passed in parameter and the internal width
	setBoundaries(mapLeftBoundary + INTERNAL_WIDTH/2, mapRightBoundary - INTERNAL_WIDTH/2);
	entity = NULL;
	//By default set the position to the minimum value possible of the camera on the map
	setPositionX(getMinPosition());
	state = IDLE;
}

//Checking the direction to take to go to the entity
void Camera::checkDirectionToTake(int posX)
{
	if(posX > getPositionX())
		direction = DIRECTION_RIGHT;
	else
		direction = DIRECTION_LEFT;
}

//Teleport to a position
void Camera::teleportTo(int posX)
{
	state = IDLE;
	move(posX);
}

//Check if we can translate to this position depending on the direction (left or right)
bool Camera::CanTranslateTo(int nextPosX)
{
	if(direction == DIRECTION_RIGHT)
	{
		if(nextPosX < getMaxPosition())
			return true;
		else
			return false;
	}
	else
	{
		if(nextPosX > getMinPosition())
			return true;
		else
			return false;
	}
}

//Teleport the camera to the position given in parameter
void Camera::move(int posXToTp)
{
	//Checking for the direction first
	checkDirectionToTake(posXToTp);

	int moveToPosX = 0;
	
	//Are we teleporting to the left or to the right, if right then
	if(direction == DIRECTION_RIGHT)
	{
		//if we can teleport to this position then go to it
		if(CanTranslateTo(posXToTp))
		{
			moveToPosX = posXToTp - getPositionX();
			glTranslatef(-moveToPosX, 0.0, 0.0);
			setPositionX(posXToTp);
		}
		//if not teleport to the maximum position of the camera
		else
		{
			moveToPosX = getMaxPosition() - getPositionX();
			glTranslatef(-moveToPosX, 0.0, 0.0);
			setPositionX(getMaxPosition());
		}
	}
	//if left then
	else
	{
		//if we can teleport to this position then go to it
		if(CanTranslateTo(posXToTp))
		{
			moveToPosX = getPositionX() - posXToTp;
			glTranslatef(moveToPosX, 0.0, 0.0);
			setPositionX(posXToTp);
		}
		//if not teleport to the minimum position of the camera
		else
		{
			moveToPosX = getPositionX() - getMinPosition();
			glTranslatef(moveToPosX, 0.0, 0.0);
			setPositionX(getMinPosition());
		}
	}
}

//The function to tell the camera to follow this entity
void Camera::followEntity(Entity* entityToFollow)
{
	state = FOLLOW;
	entity = entityToFollow;
}
void Camera::stopFollowing()
{
	state = IDLE;
	entity = NULL;
}

//Called each frame to update the camera
void Camera::update()
{
	//If the camera is following an entity then
	if(state == FOLLOW)
		move(entity->getPositionX());
}