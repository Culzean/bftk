#include "User.h"

User::User(const char iclass[])
{
	playerClass = iclass;
	//Position X and Y of the hero will be defined using the spawnpoint class but for now let use these values
	pHero = NULL;
}

//Function to initialise the hero the player will play with so in menu it'll be NULL until the player choose its hero
void User::InitHero(int posX, int posY)
{
	pHero = new Warrior(posX, posY);
	pHero->setFoe(false);
	pHero->setAttackDamage(10);
}

//Hero mouvement functions
void User::moveHero(char leftOrRight)
{
	getHero()->setFriction(1);
	getHero()->move(leftOrRight);
}

void User::stopHero(char leftOrRight)
{
	if(leftOrRight == 'l' || leftOrRight == 'r')
		getHero()->setFriction(0.43);
}

void User::heroClimb(char upOrDown)
{
	getHero()->climb(upOrDown);
}

//Attack function
void User::heroAttack()
{
	getHero()->attack();
}

//Updating the hero animation
void User::update()
{
	getHero()->update();
}

//Updating the screen with the new data for the hero
void User::show()
{
	getHero()->draw();
}

