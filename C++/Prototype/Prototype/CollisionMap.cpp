#include "CollisionMap.h"
#include "Unit.h"


CollisionMap::CollisionMap(Unit* player, Level* lvl)
{
	currentPlayer = player;
	currentLvl = lvl;
	currentSize = currentLvl->getLevelSize();

	collisionMap.reserve(currentSize * 2);
	for(int i = 0 ; i < (currentSize * 2) ; i++)
	{
		vector< Unit* > square;
		collisionMap.push_back(square);
		for(int j = 0 ; j < MAX_UNIT_DENSITY ; j++)	
		collisionMap[i].push_back(NULL);
	}
	collisionMap[currentPlayer->getMapColumn()][0] = currentPlayer;
	cout << "vector size  - " << collisionMap.size();
}

CollisionMap::~CollisionMap()
{

}

void CollisionMap::updateMap(int searchRef)
{//update top and bottom seperatley
	int searchTo = currentLvl->getLevelSize() * 2;

	//if(searchRef >= currentLvl->getLevelSize())
	//	searchTo = currentLvl->getLevelSize() * 2;
	//else
	//	searchTo = currentLvl->getLevelSize();

	for(int search = searchRef ; search < searchTo; search++)
	{
		for(int j = 0 ; j < collisionMap[search].size() ; j++)
		{
		if(collisionMap[search][j] != NULL)
			{
				if(collisionMap[search][j]->getActivity() == DIE)
				{
					collisionMap[search][j] = NULL;
					cout << "SET TO NULL" << endl;
				}
				else if(collisionMap[search][j]->getMapColumn() != search)
				{
					Unit* unitToMove = collisionMap[search][j];
					int newLocation = -1;
					int square = 0;
					do{
						if(collisionMap[unitToMove->getMapColumn()][square]==NULL)
							newLocation = unitToMove->getMapColumn();
						else
							square++;
						if(square > MAX_UNIT_DENSITY)
							break;
					}while(newLocation < 0);
					
					if(newLocation < 0)//have not found a space in the new square for this unit. it is too crowded
						unitToMove->setPositionX(search * MAX_TILE_SIZE);
					else
					{
						collisionMap[newLocation][square] = unitToMove;
						collisionMap[search][j] = NULL;
					}
				}
			}
		}
	}
}

void CollisionMap::updateFormations(vector< GroupFormation* > &ptrFormations)
{
	for(int i = 0; i < NO_FORMATIONS ; i++)
	{
		GroupFormation* currentFormation = ptrFormations[i];
			
		if(currentFormation != NULL)
			currentFormation->resetVelocity();

		if(currentFormation->getCount() > 0)
		{

		switch(currentFormation->getOrders())
			{
				
			case (guarding):

				//cout << "how many troops? "<< currentFormation->getCount() << "who ?" << currentFormation->getReg_() << endl;
				maintainForm(currentFormation);				

				break;

			case (attacking):
				//should we go back to guarding?
				//EZLOGGER ("how many troops? ", currentFormation->getCount());
				//EZLOGGER ("who ?", currentFormation->getReg_());
				if(!(attackCheck(currentFormation)))
					currentFormation->setOrders(guarding);

				break;

			default:

				break;
			}
		}
	}
}


void CollisionMap::addToMap(Unit* newUnit)
{
	int unitColumn;

	if(CollisionControl::onTop(newUnit->getMapRow()))//top floor
		{
			unitColumn = newUnit->getMapColumn() + currentSize;
			newUnit->setOnTop(true);
		}
	else
		{//on bottom floor
		unitColumn = newUnit->getMapColumn();
		newUnit->setOnTop(false);
		}
		
	int square = 0;
	do{
		if(collisionMap[unitColumn][square]==NULL)
			{
				collisionMap[unitColumn][square] = newUnit;
				newUnit = NULL;
			}
		else
			square++;
		if(square > MAX_UNIT_DENSITY)
			{
				unitColumn++;
				square = 0;
			}
	}while(newUnit!=NULL);
}

void CollisionMap::bumpCheck(bool right, int mapRef, Unit* currentUnit)
{
	int startCheck = mapRef;
	int endCheck = mapRef;
	if(right)
		endCheck++;
	else
		startCheck--;
	
	for(int j = startCheck; j < endCheck; j++)
		{
			//cout << "****  " << j << "  ^^^^^";
			//make sure no checks are outside the level bouyds
			if(j >= 0 && j < (currentSize * 2))
			{
					//need to do a group check
					
				for(int k=0; k<collisionMap[j].size(); k++)
				{
					if(collisionMap[j][k] != NULL)
						{
							if(collisionMap[j][k]->getActivity() != DIE)
							{
						if(currentUnit->getFoe() != collisionMap[j][k]->getFoe())
						{
						
						//resolve combat

							//what range?
							int dist = currentUnit->getPositionX() - collisionMap[j][k]->getPositionX();
							if(abs(dist) <= currentUnit->getWidth())
							{
								if(abs(currentUnit->getVelocityX()) < abs(collisionMap[j][k]->getVelocityX()))
								{
									if(abs(currentUnit->getVelocityX()) > MAX_VELOCITY * 0.3)
									{
										currentUnit->setLife(currentUnit->getLife() - 1);
										if(dist > 0)
										currentUnit->setPositionX(currentUnit->getPositionX()
										- (dist - currentUnit->getWidth()) * 0.2);
										else
											currentUnit->setPositionX(currentUnit->getPositionX()
											+ (dist - currentUnit->getWidth()) * 0.2);
									}
								}else
								{
									if(abs(collisionMap[j][k]->getVelocityX()) > MAX_VELOCITY * 0.3)
									{
										collisionMap[j][k]->setLife((collisionMap[j][k]->getLife()) -1);
										if(dist > 0)
										collisionMap[j][k]->setPositionX(collisionMap[j][k]->getPositionX()
										+ (dist - currentUnit->getWidth()) * 0.2);
										else
											collisionMap[j][k]->setPositionX(collisionMap[j][k]->getPositionX()
											- (dist - currentUnit->getWidth()) * 0.2);
									}
								}
							}
							else if(abs(dist) <= currentUnit->getAttackRange())
							{
								//currentUnit->setVelocityX(0);
								currentUnit->setTarget(collisionMap[j][k]);
							}
							
						}
					}
					}
				}
			}
		
		}
}

bool CollisionMap::attackCheck(GroupFormation* currentFormation)
{
	Unit* lastFriend = NULL;
	Unit* lastFoe = NULL;
	bool setTarget = false;
	int rangeCount = 7;//currentFormation->getWidth() + (currentFormation->getWidth() * 0.2);
	int runningCount = 0;
	int recrLevel = 0;
	int centreCol = currentFormation->getCentre() / MAX_TILE_SIZE;
	int searchCol = centreCol;

	do{
					//until we have exhuasted the range of the search.
					//feedDump(prowlUnit,searchCol,recrLevel);
					if(searchCol < 0)
						searchCol = 0 + recrLevel;
					else if(searchCol > currentSize * 2)
						searchCol = currentSize * 2;

			for(int k = 0; k < collisionMap[searchCol].size() ; k++)
			{
				if(collisionMap[searchCol][k] != NULL)
					{
						//EZLOGGER ("target placed?  ", collisionMap[searchCol][k]->getTarget());
						if(collisionMap[searchCol][k]->getFoe() == false)
							lastFriend = collisionMap[searchCol][k];
						else
							lastFoe = collisionMap[searchCol][k];

							if((lastFriend != NULL) && (lastFoe != NULL))
							{
								//cout << "last foe last friend"  << endl;
								lastFriend->setTarget(lastFoe);
								lastFoe->setTarget(lastFriend);
									setTarget = true;
									lastFriend = NULL;
									lastFoe = NULL;
									break;
							}
					}
			}
						searchCol = getNextCol(searchCol, centreCol, ++recrLevel);
						//cout << "where are you looking? ?  " << searchCol << endl;
					}while(runningCount++ < (rangeCount));
					return setTarget;
}

void CollisionMap::maintainForm(GroupFormation* currentFormation)
{//this could be done using recursion
	int formationCount = currentFormation->getCount();
	int runningCount = 0;
	int recrLevel = 0;
	int centreCol = currentFormation->getCentre() / MAX_TILE_SIZE;
	int searchCol = centreCol; 
	do{
			//until we find every man from this formation
			//cout << "  on guard  - " << runningCount << "  " << formationCount << endl;
			if(searchCol < 0)
				searchCol = 0 + recrLevel;
			else if(searchCol > currentSize * 2)
				searchCol = (currentSize * 2) - recrLevel;
			

			//feedDump(currentFormation);

			//cout << "**Gr8T!&&  " << currentFormation->getReg_() << "  " << searchCol << "  " << currentSize * 2;

				for(int k = 0; k < collisionMap[searchCol].size() ; k++)
				{
					//cout << "**Gr8T!&&  " << searchCol << endl;
					if(collisionMap[searchCol][k] != NULL)
					{
						if(collisionMap[searchCol][k]->getFoe() != currentFormation->getFoe())
						{
									if(CollisionControl::objectOverlap
										(currentFormation->getCentre(),collisionMap[searchCol][k]->getPositionX(),currentFormation->getWidth()))
									{
									currentFormation->setTarget(collisionMap[searchCol][k]);
									currentFormation->setOrders(attacking);
									}
						}
						else
						{
							runningCount++;
							currentFormation->addGroupVelocity(collisionMap[searchCol][k]->getVelocityX());
							if(recrLevel > currentFormation->getWidthTiles())
							{
								if(currentFormation->addSentryCount())
									collisionMap[searchCol][k]->setMarch(CollisionControl::goRight(searchCol, centreCol));
							}
						}
					}//end NULL check
				}
				if((recrLevel > 20))
					break;
				searchCol = getNextCol(searchCol, centreCol, ++recrLevel);
		}while(runningCount < formationCount);
}

bool CollisionMap::findTarget(Unit* prowlUnit)
{
	int rangeCount = 7;
	int runningCount = 0;
	int recrLevel = 0;
	int centreCol = currentPlayer->getMapColumn();
	int searchCol = currentPlayer->getMapColumn();
	
	bool makeSwap = false;
					do{
					//until we have exhuasted the range of the unit's search. Target selection is random
					//feedDump(prowlUnit,searchCol,recrLevel);
					if(searchCol < 0)
						searchCol = 0 + recrLevel;
					else if(searchCol > currentSize * 2)
						searchCol = currentSize * 2;

						for(int k = 0; k < collisionMap[searchCol].size() ; k++)
						{
						if(collisionMap[searchCol][k] != NULL)
							{
								if(collisionMap[searchCol][k]->getFormationRef() != prowlUnit->getFormationRef())
								{
									if(prowlUnit->getFoe() != collisionMap[searchCol][k]->getFoe())
											{
												prowlUnit->setTarget(collisionMap[searchCol][k]);
												makeSwap = true;
												break;
											}
								}
							}
						}
						searchCol = getNextCol(searchCol, centreCol, ++recrLevel);
						//cout << "where are you looking? ?  " << searchCol << endl;
					}while(runningCount++ < (rangeCount));
					return makeSwap;

}

bool CollisionMap::playerCheck()
{
	bool playerChangedFloor = false;
	int mapRef;
	if(currentPlayer->getOnTop())
		mapRef = currentPlayer->getMapColumn() + currentSize;
	else
		mapRef = currentPlayer->getMapColumn();
	if(currentPlayer->getIsAttacking())
	{
		//how do I find which direction the player is facing?
		int startCol = currentPlayer->getMapColumn() - 1;
		int endCol = currentPlayer->getMapColumn() + 1;
		for(int i = startCol; i < endCol ; i++)
		{
			for(int j = 0; j <collisionMap[i].size() ; j++)
			{
				if(collisionMap[i][j] != NULL)
				{
					if(collisionMap[i][j]->getFoe() != currentPlayer->getFoe())
						collisionMap[i][j]->setLife(collisionMap[i][j]->getLife() - currentPlayer->getAttackDamage());
				}
			}
		}
	}
	if(currentPlayer->getVelocityX() < 0)
		bumpCheck(false,mapRef,currentPlayer);
	else
		bumpCheck(true,mapRef,currentPlayer);
	if(currentPlayer->isClimbing)
	{
		if(currentPlayer->getOnTop() != CollisionControl::onTop(currentPlayer->getMapRow()))
		{//player needs to be replaced in collisionMap
			if(updatePlayerFloor(CollisionControl::onTop(currentPlayer->getMapRow())))
				{
					playerChangedFloor = true;
					currentPlayer->setOnTop(CollisionControl::onTop(currentPlayer->getMapRow()));
				}
				
		}
	}
	if(currentPlayer->isOnFloor)
	{
		if(currentPlayer->getOnTop() != CollisionControl::onTop(currentPlayer->getMapRow()))
		{//player needs to be replaced in collisionMap
			if(updatePlayerFloor(CollisionControl::onTop(currentPlayer->getMapRow())))
				{
					playerChangedFloor = true;
					currentPlayer->setOnTop(CollisionControl::onTop(currentPlayer->getMapRow()));
				}
		}
	}
	return playerChangedFloor;
}

bool CollisionMap::updatePlayerFloor(bool onTopNow)
{//nested for loops to search for player's old pointer - set to NULL, place new pointer upstairs/downstairs
	int newLocation;
	int searchTo = currentLvl->getLevelSize() * 2;
	int searchRef = 0;
	bool worked = false;
	if(onTopNow)
		newLocation = currentPlayer->getMapColumn() + (currentSize);
	else
		newLocation = currentPlayer->getMapColumn();
	for(int search = searchRef ; search < searchTo; search++)
		{
		for(int j = 0 ; j < collisionMap[search].size() ; j++)
			{
				//if(collisionMap[search][j] != NULL)
				//	{
				//		cout<< "    "  << (collisionMap[search][j]->getName()) << endl;
				//		if(collisionMap[search][j]->getName() == currentPlayer->getName())
				//		{
				//			int square = 0;
				//				do{//if there is no space in this square just return false and look again later
				//					if(collisionMap[newLocation][square]==NULL)
				//						worked = true;
				//					else
				//						square++;
				//					if(square > MAX_UNIT_DENSITY)
				//						break;
				//				}while(worked == false);//or break if there is no space

				//			if(worked)//have not found a space in the new square for this unit. it is too crowded
				//			{
				//				collisionMap[newLocation][square] = currentPlayer;
				//				collisionMap[search][j] = NULL;
				//			}
				//		}
				//	}
			}
		}
	cout << "where we placing the player?  - " << newLocation;
	return true;
	return worked;
}

int CollisionMap::getNextCol(int preCol, int centre, int recrLvl)
{	
	if(preCol <= centre)
			preCol = (preCol + recrLvl);
	else
			preCol = preCol - recrLvl;
	return preCol;
}

/*void CollisionMap::feedDump(GroupFormation* current)
{

	EZLOGGERSTREAM << "****************Start Feed Dump ************" << " " << std::endl;
	EZLOGGERSTREAM << std::endl;
	EZLOGGERSTREAM <<  " " << std::endl;
	EZLOGGERSTREAM <<  " " << std::endl;
	EZLOGGER ("group plus centre ", current->getReg_(), current->getCentre());
	EZLOGGER ("group count and strength ", current->getCount(), current->getStrength());
	EZLOGGER ("target and orders ", current->getGroupTarget(), current->getOrders());
	EZLOGGER ("formation size and troop march dist ", current->getFormationSize(), current->getMarchDist());
	EZLOGGER ("left/right columns ", current->getLeftColumn(), current->getRightColumn());
	EZLOGGER ("spawnX/Y ", current->getSpawnX(), current->getSpawnY());
	EZLOGGERSTREAM << "****************End Feed Dump ************" << " " << std::endl;
	EZLOGGERSTREAM << " " << std::endl;
	EZLOGGERSTREAM << std::endl;
	EZLOGGERSTREAM << " " << std::endl;

}
//
//void *///CollisionMap::feedDump(Unit* unit, int search, int recr)
//{
//
//	EZLOGGERSTREAM << "****************Start Feed Dump ************" << " " << std::endl;
//	EZLOGGERSTREAM << std::endl;
//	EZLOGGERSTREAM <<  " " << std::endl;
//	EZLOGGERSTREAM <<  " " << std::endl;
//	EZLOGGER ("where is he ? x/y ", unit->getMapColumn(), unit->getMapRow());
//	EZLOGGER ("what activity and any target?  ", unit->getActivity(), unit->getTarget());
//	EZLOGGER ("isFoe? plus formation ", unit->getFoe(), unit->getFormationRef());
//	EZLOGGER ("is attacking? plus marching orders ", unit->getIsAttacking(), unit->getMarch());
//	EZLOGGER ("range and damage ", unit->getAttackRange(), unit->getAttackDamage());
//	EZLOGGER ("left and isOnTOp? ", unit->getLife(), unit->getOnTop());
//	EZLOGGER ("And the search Algo search Col,how many iterations ", search, recr);
//	EZLOGGERSTREAM << "****************End Feed Dump ************" << " " << std::endl;
//	EZLOGGERSTREAM << " " << std::endl;
//	EZLOGGERSTREAM << std::endl;
//	EZLOGGERSTREAM << " " << std::endl;
//
//}