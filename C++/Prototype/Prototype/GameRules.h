#ifndef GAMERULES_H
	#define GAMERULES_H

	#include "Game.h"
	#include "StateManager.h"
	#include "IntroState.h"

	class PlayState;

	class GameRules
	{
		public:
			//Constructor
			GameRules(int gametype, int hp);

			//Update function
			void update(int currentPlayerLife, StateManager* stateManager);

			void win(StateManager* stateManager);
			void loose(StateManager* stateManager);

			int getEnnemyBaseHp() { return ennemyBaseHP; }
			void setEnnemyBaseHp(int newVal) { ennemyBaseHP = newVal; }
		private:
			int ennemyBaseHP, gameType;
			StateManager* pToStateManager;
	};
#endif