#ifndef LEVEL_H
	#define LEVEL_H

	#define LEVEL_HEIGHT 10 // the rows
	#define LEVEL_MAX_WIDTH 50 // the columns

	#define LEVEL_SMALL 30
	#define LEVEL_MEDIUM 40
	#define LEVEL_LARGE 50

	#define STRING_SIZE 50

	#include "Game.h"
	#include "TileModel.h"
	#include "Background.h"
	#include "PlayerSpawnPoint.h"

	class Level
	{
		public:
			Level();
			Level(char fileName[]);

			//Get and set for the size of the level
			void setLevelSize(int newLevelSize) { levelSize = newLevelSize; };
			int getLevelSize() { return levelSize; };

			//Get and set functions for the tilesheet filename
			void setTileSheetName(char newName[]) { sheetFileName = newName; };
			char* getTileSheetName() { return sheetFileName; };

			//Get and set functions for the filename attribute
			void setFileName(char newName[]) { fileName = newName; };
			char* getFileName() { return fileName; };

			//Return the tile at the position given in parameter
			TileModel& getTile(int tileX, int tileY);

			PlayerSpawnPoint* getPlayerSpawnPoint() { return pSpawnPoint; }

			//Function to init the level
			void load(char fileName[]);

			//Function to draw it on the screen each frame
			void draw();
			void update(int bgPosition);
			int startColumn, endColumn;
		private:
			char* nextLevelFileName;
			int levelSize;
			PlayerSpawnPoint* pSpawnPoint;
			char* fileName;
			char* sheetFileName;
			Bitmap* tileSheet;
			Background* background;
			TileModel tiles[LEVEL_HEIGHT][LEVEL_MAX_WIDTH];
	};
#endif