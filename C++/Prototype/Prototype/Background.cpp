#include "Game.h"
#include "Background.h"

Background::Background(const char fileName[]):Bitmap(fileName, 1, false)
{
	posX = 0;
	textStart = 0;
}

void Background::update(int newPosition)
{
	int oldRange = 1600 - 0;
	int newRange = getWidth() - 0;
	int step = newPosition/1600;
	posX = newPosition;
	textStart = (newPosition*newRange)/oldRange + 0;
}

void Background::drawBackground()
{
	drawAt(posX, 0, textStart, 0, getWidth(), getHeight());
}