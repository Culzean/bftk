#ifndef GAMESTATE_H
	#define GAMESTATE_H

	#include "StateManager.h"

	class GameState
	{
		public:
			GameState() { }
			virtual void Init() = 0;
			virtual void Cleanup() = 0;

			virtual void Pause() = 0;
			virtual void Resume() = 0;

			virtual void HandleKeyboardEvents(unsigned char value, int x, int y, StateManager* stateManager) = 0;
			virtual void HandleSpecialKeyboardEvents(int value, int x, int y, StateManager* stateManager) = 0;
			virtual void HandleSpecialKeyboardUpEvents(int value, int x, int y, StateManager* stateManager) = 0;
			virtual void HandleMouseEvent(int button, int state, int x, int y, StateManager* stateManager) = 0;

			virtual void Update(StateManager* stateManager) = 0;
			virtual void Draw() = 0;
	};
#endif