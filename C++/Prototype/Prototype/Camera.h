/*--------------------------------------------------------------------------------------------------------------
	Copyright (C) 2011  Raphael Chappuis

						This class is used to manage the camera translation

	It'll translate according to the position given in the args.
	Map boundaries and window width need to be defined in order to know if the camera 
	can move or not.
	You can also teleport the camera to a certain point using the function teleportTo().
	It can be useful if you want to disable user control and teleport the camera to an
	entity or point on the map (i.e for cutscene).

	This camera was developped only to be used in 2d game that need an horizontal scroll.

--------------------------------------------------------------------------------------------------------------*/

#ifndef CAMERA_H
	#define CAMERA_H

	#include "Game.h"
	#include "Entity.h"

	class Camera
	{
		public:
			//Enum for the direction
			enum DIRECTION { DIRECTION_LEFT, DIRECTION_RIGHT } direction;
			enum STATE { IDLE, FOLLOW } state;

			//Constructor
			Camera(int mapLeftBoundary, int mapRightBoundary);

			//Get and set for position of the camera
			void setPositionX(int newPositionX) { posX = newPositionX; };
			int getPositionX() { return posX; };

			//Get and set for position of the camera
			void setBoundaries(int newMinPosition, int newMaxPosition) { minPositon = newMinPosition; maxPosition = newMaxPosition; };
			int getMinPosition() { return minPositon; };
			int getMaxPosition() { return maxPosition; };

			//Check if the camera can translate on the current direction
			bool CanTranslateTo(int posX);

			//Check the direction the camera need to take to go to the point
			void checkDirectionToTake(int posX);

			//Check the direction to take and teleport to the position
			void teleportTo(int posX);

			//Move the camera to the position passed in argument if on the bounds of the map
			void move(int posXToTp);

			//Follow functions
			void followEntity(Entity* entity);
			void stopFollowing();

			//Update function
			void update();
		private:
			int posX;
			int minPositon, maxPosition;
			Entity* entity;
	};
#endif