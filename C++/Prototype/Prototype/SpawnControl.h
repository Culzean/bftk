#ifndef SPAWNCONTROL_H
#define SPAWNCONTROL_H

#include "Entity.h"
#include "CollisionMap.h"
#include "AIFootman.h"
#include "DecisionFramework.h"
#include "GroupFormation.h"
#include "Level.h"
#include "Flag.h"
#include "GameRules.h"
#include <vector>

#define SPAWN_COUNT 8000

#define UNIT_LIMIT 50

#define NO_FORMATIONS 5

#define SQUAD_COUNT 6

struct TriggerPoints
{
	bool tripped; int positions;
	TriggerPoints(){ tripped = false; };
};

class SpawnControl
{
public:
	SpawnControl(Level* currentLevel, Unit* player, GameRules* setRules);
	~SpawnControl();

	void startLevel(Level* lvl, Unit* player);
	void addEntity(int formation, bool friendOrFoe);
	void removeEntity(int unitRef);
	void removeEntity(int unitRef, int formation);
	void deleteEntity();
	void addSquad(int formation,bool enemy);

	void draw();
	void update();

private:
	int counter;
	int whichAI;
	int playerIncr, enemyIncr;
	int playerCount, enemyCount;
	int playerTrig, enemyTrig;

	void processOrders();
	void resetCounters();
	void updateCounters();
	void createDecisionFramework();
	void createFlags();//this information should be read from level file
	void setTriggers();
	bool removeCount();
	void resetTargets();
	int NO_TRIGGERS;
	int NO_FLAGS;

	//To handle a vector of units
	vector< AIFootman* > rostar;
	vector< AIFootman* > coffin;
	Unit* currentPlayer;
	Level* currentLvl;
	//Flag* testFlag;
	int currentSize;
	int totalCount;
	int thisTurn;
	int deathTimer;

	GameRules* rules;

	//CollisionMap* lvlMap;

	//for making a decision
	CDecisionFramework* DF_attack;
	CDecisionFramework* DF_run;

};

#endif