
#include "GroupFormation.h"
#include "Level.h"

//list of sizes for formations
int formationStrength[FORMATION_DEPTH] = {0, 1, 2, 4, 7, 12, 33, 54};

GroupFormation::GroupFormation
	(int formationReg,int width, int centre, int strength, int unitCount,ORDERS startFormation,int formationSize, int spawnX, int spawnY)
{
		reg_ = formationReg;
		strength_ = strength;
		centre_ = (centre);
		width_ = (width);
		left_ = (centre - (width) / 2);
		right_ = (centre + (width) / 2);
		formationSize_ = (formationSize);
		currentOrders = (startFormation);
		previousOrders = (startFormation);
		unitCount_ = 0;
		addUnit(unitCount);
		spawnPoint.x_ = spawnX;
		spawnPoint.y_ = spawnY;
		setTarget(NULL);
		setGroupTarget(NULL);
}


void GroupFormation::addUnit(int count)
{
	cout << "  ReGestered  " << unitCount_;

	unitCount_ += count;

	//no zeros!
	if(unitCount_ > 0)
	{
		if(formationSize_ > (FORMATION_DEPTH - 1))
		{
				cout << "too many units in this group";
		}
		else
		{
			cout << "plus plus";
			if(unitCount_ > formationStrength[(formationSize_ + 1)])
			{
				//cehck if there are enough troops to warrent taking more space
				width_ += 1 * MAX_TILE_SIZE;
				formationSize_++;
				cout << "  Formation " << formationStrength[(formationSize_ + 1)] << " has added width  " << unitCount_;
			}
			marchDist_ = (((unitCount_ * MAX_TILE_SIZE) - getWidth())/ unitCount_);
			SENTRYTRIGGER = (marchDist_ / MAX_VELOCITY);
			sentryCount = SENTRYTRIGGER;
			strength_ = formationStrength[unitCount_] * unitCount_;

		}
	}
}

void GroupFormation::removeUnit(int count)
{
	unitCount_ -= count;
	cout << " DEATH  - " << count << "  " << unitCount_ << "  " << getReg_();

	if(unitCount_ < 0)
	{
		cout << "!!!Too many units removed!!";
	}else
	{

	if(unitCount_ < formationStrength[(formationSize_ - 1)])
		{
			//cehck if there are fewer troops now. shrink the amount of space they take
			width_ -= 1 * MAX_TILE_SIZE;
			formationSize_--;
			cout << "Formation " << reg_ << " has added width";
		}
		
		sentryCount = 0;
		if(unitCount_ <= 0)
		{
			cout << "There are no units left in this group  " << reg_;
			unitCount_ = 0;
			marchDist_ = 1;
			strength_ = formationStrength[formationSize_] * 1;
			SENTRYTRIGGER = 0;

		}else
		{
			marchDist_ = ((width_ - (unitCount_ * MAX_TILE_SIZE))/ unitCount_);
			strength_ = formationStrength[formationSize_] * unitCount_;
			SENTRYTRIGGER = unitCount_ * 2;
		}
	}
		
}

void GroupFormation::setWidth(int width)
{
	if(unitCount_ == 1)
	{
		width_ = 1;
	}else
	{
		width_ = width;
	}
	//might want to check some level details here
	setCentre(centre_);
}

void GroupFormation::setCentre(int centre)
{

	if(centre < 0)
	{
		centre_ = 0;
	}else if(centre > (LEVEL_MAX_WIDTH * MAX_TILE_SIZE))
	{
		centre_ = (LEVEL_MAX_WIDTH * MAX_TILE_SIZE);
	}else
	{
		centre_ = centre;
	}

	left_ = centre - (width_ /2);
	right_ = centre + (width_ / 2);
}

void GroupFormation::setOrders(ORDERS newOrders)
{
	previousOrders = currentOrders;
	currentOrders = newOrders;
}

bool GroupFormation::addSentryCount()
{
	bool changeDir = false;
	sentryCount++;
	//cout << "information " << sentryCount << "  " << SENTRYTRIGGER << "  ";
	if(sentryCount > SENTRYTRIGGER)
		{
			changeDir = true;
			sentryCount = 0;
		}
	return changeDir;
}

int GroupFormation::getLeftColumn()
{
	int centreColumn = getCentre() / MAX_TILE_SIZE;

	int halfWidthColumn = (getWidth() / 2) / MAX_TILE_SIZE;

	int leftMost = centreColumn - halfWidthColumn;

	return leftMost;
}

int GroupFormation::getRightColumn()
{
	int centreColumn = getCentre() / MAX_TILE_SIZE;

	int halfWidthColumn = (getWidth() / 2) / MAX_TILE_SIZE;

	int rightMost;
	if((getWidth() / MAX_TILE_SIZE) % 2 == 1)
		rightMost = centreColumn + halfWidthColumn + 1;
	else
		rightMost = centreColumn + halfWidthColumn;

	return rightMost;
}