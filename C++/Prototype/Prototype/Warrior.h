#ifndef WARRIOR_H
	#define WARRIOR_H

	#include "Unit.h"

	class Warrior: public Unit
	{
		public:
			//Constructor
			Warrior(int posX, int posY);

			//Custom update for the mushroom
			void update();
	};
#endif