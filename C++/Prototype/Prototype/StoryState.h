#ifndef STORYSTATE_H
	#define STORYSTATE_H

	#include "GameState.h"
	#include "Bitmap/Bitmap.h"
	#include "TextDisplayer.h"

	class StoryState: public GameState
	{
		public:
			StoryState();
			void Init();
			void Cleanup();

			void Pause();
			void Resume();

			//Events handling functions
			void HandleKeyboardEvents(unsigned char value, int x, int y, StateManager* stateManager);
			void HandleSpecialKeyboardEvents(int value, int x, int y, StateManager* stateManager);
			void HandleSpecialKeyboardUpEvents(int value, int x, int y, StateManager* stateManager);
			void HandleMouseEvent(int button, int state, int x, int y, StateManager* stateManager);

			void Update(StateManager* stateManager);
			void Draw();
		private:
			Bitmap* storyBackground;
	};
#endif