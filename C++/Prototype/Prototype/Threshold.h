// Threshold.h: interface for the CThreshold class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_THRESHOLD_H__E4C4BF6D_B212_4A3E_99D2_D0C04A270A9D__INCLUDED_)
#define AFX_THRESHOLD_H__E4C4BF6D_B212_4A3E_99D2_D0C04A270A9D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <assert.h>
#include <math.h>

#define Assert(a,b) assert( a && b)

//#include "DecisionFramework.h"
//#include "Threshold.h"

typedef unsigned int UINT;

typedef enum {
	THRESH_SLOPE_DOWN	= -1,
	THRESH_SLOPE_HORIZ	= 0,
	THRESH_SLOPE_UP		= 1,
	THRESH_SLOPE_VERT	= 2} THRESH_SLOPE_ORIENT;	// The current orientation of the threshold
													// Used to determine the proper corner point

typedef enum {
	THRESH_RESULT_UNDER = 0,
	THRESH_RESULT_OVER	= 1} THRESH_RESULT_DIRECTION;	// Which side of the threshold line is the result zone

typedef enum {
	THRESH_GRADE_BINARY = 0,
	THRESH_GRADE_PERP,
	THRESH_GRADE_HORIZ,
	THRESH_GRADE_VERT,
	THRESH_GRADE_CUSTOM} THRESH_GRADE_TYPE;			// The direction of the gradient curve

typedef enum {
	THRESH_DISTANCE_LINEAR = 0,
	THRESH_DISTANCE_PERCENT} THRESH_DISTANCE_TYPE;		// How to measure distance into the result zone

typedef enum {
	THRESH_NUDGE_PERP = 0,
	THRESH_NUDGE_HORIZ,
	THRESH_NUDGE_VERT,
	THRESH_NUDGE_CUSTOM} THRESH_NUDGE_DIRECTION;		// Which direction to move the threshold in "nudge" functions


struct DATA_POINT {
	double X;
	double Y;
};														// The commonly used structure for points

class CDecisionFramework;

class CThreshold  
{
public:

	//////////////////////////////////////////////////////////////////////
	// Public Accessors

	//////////////////////////////////////////////////////////////////////
	// Set Intercept Points

	void SetYInt( double Y );
	void SetXInt( double X );
	void SetXandY( double X, double Y );

	void SetSlopeAndY( double Slope, double Y );
	void SetSlopeAndX( double Slope, double X );

	void SetTopAndXInt( double Top, double X );
	void SetTopAndYInt( double Top, double Y );
	
	void SetRightAndXInt( double Right, double X );
	void SetRightAndYInt( double Right, double Y );	

	void SetTopAndRight( double Top, double Right );
	
	void SetSlopeAndTop( double Slope, double Top );
	void SetSlopeAndRight( double Slope, double Right );

	void NudgeThresh(	double Distance,  
						THRESH_DISTANCE_TYPE DistanceType = THRESH_DISTANCE_LINEAR, 
						THRESH_NUDGE_DIRECTION NudgeDirection = THRESH_NUDGE_PERP );  
						// Move the threshold the specified amount in the stated direction


	//////////////////////////////////////////////////////////////////////
	// Set Threshold Behaviors

	void SetThreshResultDirection( THRESH_RESULT_DIRECTION ResultDirection );
	void SetThreshGradientType( THRESH_GRADE_TYPE ThreshGradientType );
	void SetGradientSlope( double Slope );

	//////////////////////////////////////////////////////////////////////
	// Get Intercept Points

	double GetX() const { return mThreshXInt; } 
	double GetY() const { return mThreshYInt; }
	double GetSlope() const { return mThreshSlope; }
	double GetXatY( double Y ) const;
	double GetYatX( double X ) const;

	//////////////////////////////////////////////////////////////////////
	// Get Result Data

	bool PointInResultZone( DATA_POINT DataPoint ) const;
	
	double GetDepth( DATA_POINT DataPoint,  THRESH_DISTANCE_TYPE DistanceType = THRESH_DISTANCE_LINEAR );

	
	/////////////////////////////////////////////////////////////////////
	// Construction/Destruction

	
	CThreshold();	
	CThreshold(	CDecisionFramework* pDecFrame, 
				double XInt = 0, 
				double Yint = 0, 
 				THRESH_RESULT_DIRECTION ResultDirection = THRESH_RESULT_UNDER,
				THRESH_GRADE_TYPE GradientType = THRESH_GRADE_BINARY,
				double CustomSlope = 0 );
	
	virtual ~CThreshold();

	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////

private:

	//////////////////////////////////////////////////////////////////////
	// Private calculation functions

	double CalcYInt( DATA_POINT DataPoint, double Slope );
	double CalcXInt( DATA_POINT DataPoint, double Slope );
	void CalcXAndYInt( DATA_POINT DataPoint, double Slope  );

	double CalcSlope( DATA_POINT PointA, DATA_POINT PointB );
	double CalcSlope( DATA_POINT PointA, double X, double Y );

	void ReCalcData();

	double GetNewXIntercept() const;
	double GetNewYIntercept() const;
	double GetMaxDepth();
	double GetInverseSlope() const;

	UINT GetMaxX() const; 
	UINT GetMaxY() const;

	void ProcessSlopes();	
	void SetGradientSlope();
	void SetThreshOrientation( THRESH_SLOPE_ORIENT Orientation );
	void SetCornerPoint();

	double CalcDistanceBetweenPoints( DATA_POINT PointA, DATA_POINT PointB );
	double CalcDistanceFromThreshold( DATA_POINT Point, THRESH_GRADE_TYPE GradientType );

	DATA_POINT GetNearestThreshPoint( DATA_POINT DataPoint, double Slope );
	void SetPerpPoint();

	//////////////////////////////////////////////////////////////////////
	// Private member data
	
	CDecisionFramework* mpParentDecFrame;		// Pointer to parent DecFrame
	
	double	mThreshXInt;
	double	mThreshYInt;
	double	mThreshSlope;

	double		mGradientSlope;
	DATA_POINT	mPerpPoint;
	DATA_POINT	mCornerPoint;
	double		mMaxDepth;

	THRESH_SLOPE_ORIENT			mThreshOrientation;
	THRESH_GRADE_TYPE		mThreshGradientType;
	THRESH_RESULT_DIRECTION		mThreshResultDirection;

};


#endif // !defined(AFX_THRESHOLD_H__E4C4BF6D_B212_4A3E_99D2_D0C04A270A9D__INCLUDED_)
