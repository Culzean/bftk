/*-------------------------------------------------------------------------------------------------
	Copyright (C) 2011  Raphael Chappuis

							This class draws animated bitmaps.

	You need to pass an array of BitmapAnimation(store texture coordinates, number of frame...),
	the path to the bitmap file you want to use and if you need transparency or not.
	This class inherit from the Bitmap class(thanks to Chris Backhouse, Daniel Livingstone, 
	Alistair McMonnies).

	In this class are stored the current frame of the animation, the animations and the
	function to play one of the animation.

	This class can be used with an atlas map but each frame of the animation have to be the 
	same size (i.e 40x64px).

	Example:
	--------
	BitmapAnimated* sprite = NULL;
	BitmapAnimation idleAnimation;   // the idle animation
	idleAnimation.setTextureCoordinate(0, 0); //texture coordinate x, y of the first frame
	idleAnimation.setAnimationSpeed(0.03); //the animation speed
	idleAnimation.setFrameSize(40, 64);    //width and height of the frames
	idleAnimation.setNumberOfFrame(3);     //number of frame in the animation
		
	//Initialising the animated bitmap with a size of 1 and transparency
	sprite = new BitmapAnimated(idleAnimation, 1, "Sprites/Character/mushroom.bmp", true);

	//Call the function PlayAnimation in your display function
	sprite->PlayAnimation(sprite->getPositionX(), sprite->getPositionY());

	Use an enum to set a new animation to this bitmap depending on the activity of your entity

-------------------------------------------------------------------------------------------------*/

#ifndef BITMAP_ANIMATED_H
	#define BITMAP_ANIMATED_H

	#include "Bitmap.h"
	#include "BitmapAnimation.h"

	class BitmapAnimated: public Bitmap
	{
		public:
				//Constructors
				BitmapAnimated(double size, const char fileName[], bool transparency);
				BitmapAnimated(BitmapAnimation animation, double size, const char fileName[], bool transparency);
				BitmapAnimated(int posX, int posY, double size, const char fileName[], bool transparency);
				BitmapAnimated(int posX, int posY, BitmapAnimation animation, double size, const char fileName[], bool transparency);

				//Get and set functions for the animations
				void setAnimation(BitmapAnimation newAnimation) { animation = newAnimation; };
				BitmapAnimation getAnimation() { return animation; };

				//Get and set functions for the current frame
				void setCurrentFrame(double newCurrentFrame) { currentFrame = newCurrentFrame; };
				double getCurrentFrame() { return currentFrame; };

				//Function to play an animation from the animation array
				void PlayAnimation(int posX, int posY);
		private:
				BitmapAnimation animation;
				double currentFrame;
	};
#endif