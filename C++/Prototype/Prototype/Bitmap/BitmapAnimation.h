/*-------------------------------------------------------------------------------------------------
	Copyright (C) 2011  Raphael Chappuis

					This class is used with the BitmapAnimated class.

	In this class are stored the number of frame of the animation, the coordinate of 
	the first frame (we need the coordinates because this class have to be used with an atlas map,
	if you don't know what it is, then go to wikipedia :)), the width and the height that will
	be used for each frame (the calcul used to get the next frame coordinate in the BitmapAnimated 
	class is the following : textureCoordinateX + frameWidth*currentFrame so each frame have to be 
	the same size on you atlas map!). We can also change the speed of the animation.

	Example:
	--------
	BitmapAnimated* sprite = NULL;
	BitmapAnimation idleAnimation;   // the idle animation
	BitmapAnimation animations[1];   // the array of animations to send to the constructor
	idleAnimation.setTextureCoordinate(0, 0); //texture coordinate x, y of the first frame
	idleAnimation.setAnimationSpeed(0.03); //the animation speed
	idleAnimation.setFrameSize(40, 64);    //width and height of the frames
	idleAnimation.setNumberOfFrame(3);     //number of frame in the animation
	animations[0] = idleAnimation;         // putting the animation in the array
		
	//Initialising the animated bitmap with a size of 1 and transparency
	sprite = new BitmapAnimated(animations, 1, "Sprites/Character/mushroom.bmp", true);

	//Call the function PlayAnimation, using an enum(best solution but not done in this example) 
	//or an int, in your display function
	sprite->PlayAnimation(0);

-------------------------------------------------------------------------------------------------*/

#ifndef BITMAP_ANIMATION_H
	#define BITMAP_ANIMATION_H

	#define DEFAULT_ANIMATION_SPEED 0.2
	#define DEFAULT_NUMBER_OF_FRAME 1

	class BitmapAnimation
	{
		public:
				//Constructors
				BitmapAnimation();
				BitmapAnimation(int numberOfFrame, int textCoordinateX, int textCoordinateY, int frameWidth, int frameHeight, double animationSpeed, bool loopable);

				//Get and set functions for the animation speed attribute
				void setAnimationSpeed(double newAnimationSpeed) { animationSpeed = newAnimationSpeed; };
				double getAnimationSpeed() { return animationSpeed; };

				//Get and set functions for the number of frame attribute
				void setNumberOfFrame(int newFrameNumber) { numberOfFrame = newFrameNumber; };
				int getNumberOfFrame() { return numberOfFrame; };

				//Get and set functions for texture coordinates attributes
				void setTextureCoordinate(int newTextCoordinateX, int newTextCoordinateY) { setTextureCoordinateX(newTextCoordinateX); setTextureCoordinateY(newTextCoordinateY); };
				void setTextureCoordinateX(int newTextCoordinateX) { textCoordinateX = newTextCoordinateX; };
				void setTextureCoordinateY(int newTextCoordinateY) { textCoordinateY = newTextCoordinateY; };
				int getTextureCoordinateX() { return textCoordinateX; };
				int getTextureCoordinateY() { return textCoordinateY; };

				//Get and set functions for the frames size attributes
				void setFrameSize(int newFrameWidth, int newFrameHeight) { setFrameWidth(newFrameWidth); setFrameHeight(newFrameHeight); };
				void setFrameWidth(int newFrameWidth) { frameWidth = newFrameWidth; };
				void setFrameHeight(int newFrameHeight) { frameHeight = newFrameHeight; };
				int getFrameWidth() { return frameWidth; };
				int getFrameHeight() { return frameHeight; };
				bool loop;
		private:
				int numberOfFrame;
				int textCoordinateX;
				int textCoordinateY;
				int frameWidth;
				int frameHeight;
				double animationSpeed;
	};
#endif