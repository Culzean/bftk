/*-------------------------------------------------------------------------------------------------
	Copyright (C) 2011  Raphael Chappuis

							This class draws animated bitmaps.

	You need to pass an array of BitmapAnimation(store texture coordinates, number of frame...),
	the path to the bitmap file you want to use and if you need transparency or not.
	This class inherit from the Bitmap class(thanks to Chris Backhouse, Daniel Livingstone, 
	Alistair McMonnies).

	In this class are stored the current frame of the animation, the animations and the
	function to play one of the animation.

	This class can be used with an atlas map but each frame of the animation have to be the 
	same size (i.e 40x64px).

	Example:
	--------
	BitmapAnimated* sprite = NULL;
	BitmapAnimation idleAnimation;   // the idle animation
	idleAnimation.setTextureCoordinate(0, 0); //texture coordinate x, y of the first frame
	idleAnimation.setAnimationSpeed(0.03); //the animation speed
	idleAnimation.setFrameSize(40, 64);    //width and height of the frames
	idleAnimation.setNumberOfFrame(3);     //number of frame in the animation
		
	//Initialising the animated bitmap with a size of 1 and transparency
	sprite = new BitmapAnimated(idleAnimation, 1, "Sprites/Character/mushroom.bmp", true);

	//Call the function PlayAnimation in your display function
	sprite->PlayAnimation(sprite->getPositionX(), sprite->getPositionY());

	Use an enum to set a new animation to this bitmap depending on the activity of your entity

-------------------------------------------------------------------------------------------------*/

#include "BitmapAnimated.h"

//Constructors
BitmapAnimated::BitmapAnimated(double size, const char fileName[], bool transparency):Bitmap(fileName, size, transparency)
{
	BitmapAnimation baseAnimation = BitmapAnimation();
	setAnimation(baseAnimation);
	setCurrentFrame(0);
}
BitmapAnimated::BitmapAnimated(BitmapAnimation animation, double size, const char fileName[], bool transparency):Bitmap(fileName, size, transparency)
{
	setAnimation(animation);
	setCurrentFrame(0);
}
BitmapAnimated::BitmapAnimated(int posX, int posY, double size, const char fileName[], bool transparency):Bitmap(posX, posY, fileName, size, transparency)
{
	BitmapAnimation baseAnimation = BitmapAnimation();
	setAnimation(baseAnimation);
	setCurrentFrame(0);
}
BitmapAnimated::BitmapAnimated(int posX, int posY, BitmapAnimation animation, double size, const char fileName[], bool transparency):Bitmap(posX, posY, fileName, size, transparency)
{
	setAnimation(animation);
	setCurrentFrame(0);
}

//This function is called everytime the opengl display function is called (for me its 60 call/sec)
//Use it for animation that loop
void BitmapAnimated::PlayAnimation(int posX, int posY)
{
	int curFrame = (int)getCurrentFrame();

	//Reset the frame when the animation is at its last frame (-1 because the animation start at frame 0)
	if (curFrame > animation.getNumberOfFrame() - 1)
	{
		//If we're looping than just reset the frame counter
		if(animation.loop == true)
		{
			setCurrentFrame(0);
			curFrame = 0;
		}
		//Otherwise, just display the last frame of the animation
		else
		{
			setCurrentFrame(animation.getNumberOfFrame() - 1);
			curFrame = animation.getNumberOfFrame() - 1;
		}
	}

	//Calculate the new coordinateX depending on the current frame
	int newCoordinateX = animation.getTextureCoordinateX() + (animation.getFrameWidth() * curFrame);

	//Draw the new frame
	drawAt(posX, posY, newCoordinateX, animation.getTextureCoordinateY(), animation.getFrameWidth(), animation.getFrameHeight());

	//This define the time to go to the next frame (default is 0.2)
	setCurrentFrame(currentFrame + animation.getAnimationSpeed());
}