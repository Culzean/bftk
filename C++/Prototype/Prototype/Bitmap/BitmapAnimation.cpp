/*-------------------------------------------------------------------------------------------------
	Copyright (C) 2011  Raphael Chappuis

					This class is used with the BitmapAnimated class.

	In this class are stored the number of frame of the animation, the coordinate of 
	the first frame (we need the coordinates because this class have to be used with an atlas map,
	if you don't know what it is, then go to wikipedia :)), the width and the height that will
	be used for each frame (the calcul used to get the next frame coordinate in the BitmapAnimated 
	class is the following : textureCoordinateX + frameWidth*currentFrame so each frame have to be 
	the same size on you atlas map!). We can also change the speed of the animation.

	Example:
	--------
	BitmapAnimated* sprite = NULL;
	BitmapAnimation idleAnimation;   // the idle animation
	BitmapAnimation animations[1];   // the array of animations to send to the constructor
	idleAnimation.setTextureCoordinate(0, 0); //texture coordinate x, y of the first frame
	idleAnimation.setAnimationSpeed(0.03); //the animation speed
	idleAnimation.setFrameSize(40, 64);    //width and height of the frames
	idleAnimation.setNumberOfFrame(3);     //number of frame in the animation
	animations[0] = idleAnimation;         // putting the animation in the array
		
	//Initialising the animated bitmap with a size of 1 and transparency
	sprite = new BitmapAnimated(animations, 1, "Sprites/Character/mushroom.bmp", true);

	//Call the function PlayAnimation, using an enum(best solution but not done in this example) 
	//or an int, in your display function
	sprite->PlayAnimation(0);

-------------------------------------------------------------------------------------------------*/

#include "BitmapAnimation.h"

//Defining constructors
BitmapAnimation::BitmapAnimation()
{
	setNumberOfFrame(0);
	setTextureCoordinate(0, 0);
	setFrameSize(0, 0);
	setAnimationSpeed(0);
	loop = false;
}
BitmapAnimation::BitmapAnimation(int numberOfFrame, int textCoordinateX, int textCoordinateY, int frameWidth, int frameHeight, double animationSpeed, bool loopable)
{
	setNumberOfFrame(numberOfFrame);
	setTextureCoordinate(textCoordinateX, textCoordinateY);
	setFrameSize(frameWidth, frameHeight);
	setAnimationSpeed(animationSpeed);
	loop = loopable;
}